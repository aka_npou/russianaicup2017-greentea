import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

/**
 * User: goodg_000
 * Date: 25.11.2017
 * Time: 12:21
 */
public class PointsAxisTest {

   private static double DELTA = Utils.SMALL_DOUBLE;

   @Test
   public void test2Points1() {
      double axis = Utils.calcPointsAxis(Arrays.asList(new Point(0, 0), new Point(0, 1)));
      Assert.assertEquals(Math.PI / 2, axis, DELTA);
   }

   @Test
   public void test2Points2() {
      double axis = Utils.calcPointsAxis(Arrays.asList(new Point(0, 0), new Point(1, 0)));
      Assert.assertEquals(Math.PI, axis, DELTA);
   }

   @Test
   public void test2Points3() {
      double axis = Utils.calcPointsAxis(Arrays.asList(new Point(0, 0), new Point(1, 1)));
      Assert.assertEquals(Math.PI / 4, axis, DELTA);
   }

   @Test
   public void test4Points1() {
      double axis = Utils.calcPointsAxis(Arrays.asList(new Point(0, 0), new Point(0, 4),
              new Point(-1, 2), new Point(1, 2)));
      Assert.assertEquals(Math.PI / 2, axis, DELTA);
   }

   @Test
   public void test100PointsPerformance1() {
      Random r = new Random(2);
      List<Point> points = new ArrayList<>();
      for (int i = 0; i < 100; ++i) {
         points.add(new Point(r.nextDouble(), r.nextDouble()));
      }

      double sum = 0;
      long start = System.currentTimeMillis();
      int count = 30000;
      for (int i = 0; i < count; ++i) {
         sum += Utils.calcPointsAxis(points);
      }
      long time = System.currentTimeMillis() - start;
      System.out.println(sum);
      System.out.println(String.format("test100PointsPerformance1 (%s): %sms ", count, time));
   }
}
