/**
 * User: goodg_000
 * Date: 11.11.2017
 * Time: 13:10
 */
class GroupMoveResult {
   private final Group group;
   private Point target;
   private Point targetWithShift;

   private double scoreDiff;

   public GroupMoveResult(Group group) {
      this.group = group;
   }

   public Group getGroup() {
      return group;
   }

   public double getScoreDiff() {
      return scoreDiff;
   }

   public Point getTarget() {
      return target;
   }

   public void setTarget(Point target) {
      this.target = target;
   }

   public void setScoreDiff(double scoreDiff) {
      this.scoreDiff = scoreDiff;
   }

   public Point getTargetWithShift() {
      return targetWithShift;
   }

   public void setTargetWithShift(Point targetWithShift) {
      this.targetWithShift = targetWithShift;
   }
}
