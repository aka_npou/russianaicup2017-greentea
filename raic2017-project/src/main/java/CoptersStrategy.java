import model.Game;
import model.Move;
import model.VehicleType;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * User: goodg_000
 * Date: 07.12.2017
 * Time: 11:31
 */
public class CoptersStrategy extends BattleStrategy<CoptersStrategyInfo> {

   public CoptersStrategy(Game game, WorldInfo world, List<Group> groups, Battlefield battlefield) {
      super(game, world, groups, battlefield, false);
   }

   @Override
   protected boolean apply(Move move) {
      A: for (Group copters : getCoptersGroups()) {
         Group bmps = findNearestBmps(copters);

         if (bmps == null) {
            finishStrategyForGroups();
            return false;
         }

         List<BattleUnit> enemyFighters = world.getEnemyUnitsOfType(VehicleType.FIGHTER);
         if (copters.size() * 0.25 > enemyFighters.size()) {
            finishStrategyForGroup(copters);
            continue;
         }

         Point fightersPos = Utils.calcAveragePosOfUnits(enemyFighters);
         Point coptersPos = copters.getPos();
         Point bmpsPos = bmps.getPos();
         double distFightersToCopters = fightersPos.distanceTo(coptersPos);
         double distFightersToBmps = fightersPos.distanceTo(bmpsPos);
         double distCoptersToBmps = coptersPos.distanceTo(bmpsPos);

         double timeFightersToCopters = distFightersToCopters / Utils.getSpeed(VehicleType.FIGHTER);
         double timeFightersToBmps = distFightersToBmps / Utils.getSpeed(VehicleType.FIGHTER);
         final double reactionTime = 20;
         double timeCoptersToBmps = distCoptersToBmps / Utils.getSpeed(VehicleType.HELICOPTER) + reactionTime;

         boolean coveredByBmp = false;
         //boolean coveredByBmp = new Segment(fightersPos, coptersPos).distanceTo(bmpsPos) < bmps.getGroupRadius();

         if (!coveredByBmp && (timeCoptersToBmps > timeFightersToCopters || timeCoptersToBmps > timeFightersToBmps)) {
            List<Point> moveTargets = findMoveTargets(copters, bmps, fightersPos);
            battlefield.prepareCachesForGroup(copters);
            for (Point targetPos : moveTargets) {
               if (battlefield.isHasCollisions(copters, targetPos)/* ||
                       battlefield.calcBattleScoreOnPath(copters, targetPos) <= 0*/) {
                  continue;
               }

               if (copters.getMoveTargetWithShiftOrPos().distanceTo(targetPos) < copters.getGroupRadius()*1.5) {
                  continue A;
               }

               final Group g = copters;
               return selectAndCallTrigger(copters, move, new SelectGroupActionStrategyTrigger() {
                  @Override
                  public boolean apply(Move move) {
                     performMoveAction(g, targetPos, targetPos, move);
                     return true;
                  }
               });
            }
         }

         if (isCurrentStrategy(copters) ) {
            long finishTick = world.getTick() + 50;
            final Group g = copters;

            CoptersStrategyInfo si = getGroupStrategyInfo(g);
            if (si.getTrigger() == null) {
               si.setTrigger(new GroupStrategyTrigger() {
                  @Override
                  public boolean isShouldApply() {
                     return world.getTick() > finishTick;
                  }

                  @Override
                  public boolean apply(Move move) {
                     finishStrategyForGroup(g);
                     return false;
                  }
               });
            }
         }
      }

      return false;
   }

   private List<Group> getCoptersGroups() {
      List<Group> res = new ArrayList<>();
      for (Group g : getStrategyCandidatesWithCurrent()) {
         if (g.getVehicleType().equals(VehicleType.HELICOPTER) && g.size() >= 10) {
            res.add(g);
         }
      }
      res.sort(Comparator.comparing(Group::size).reversed());
      return res;
   }

   private Group findNearestBmps(Group copters) {
      double minDist = Integer.MAX_VALUE;
      Group res = null;
      for (Group g : groups) {
         if (g.getVehicleType().equals(VehicleType.IFV) && g.size() >= 35) {
            double dist = g.getPos().distanceTo(copters.getPos());
            if (dist < minDist) {
               res = g;
               minDist= dist;
            }
         }
      }
      return res;
   }


   private List<Point> findMoveTargets(Group copters, Group bmps, Point fightersPos) {
      List<Point> res = new ArrayList<>();
      Point coptersPos = copters.getPos();
      Point bmpsPos = bmps.getPos();
      if (bmps.getMoveTargetWithShift() != null && !bmps.getMoveTargetWithShift().equals(bmps.getPos())) {
         double timeToGoToBmps = coptersPos.distanceTo(bmpsPos) / Utils.getSpeed(VehicleType.HELICOPTER);
         double bmpsMoveDist = Utils.getSpeed(VehicleType.IFV)*timeToGoToBmps;
         Vector2D bmpsMoveVector = new Vector2D(bmps.getPos(), bmps.getMoveTargetWithShift()).setLength(bmpsMoveDist);
         bmpsPos = Utils.ensureInBoundsOfField(bmpsPos.plus(bmpsMoveVector));
      }

      Vector2D moveToBmpsVector = new Vector2D(coptersPos, bmpsPos);

      Vector2D coverVector = new Vector2D(fightersPos, bmpsPos).setLength(copters.getGroupRadius());
      Point targetPos = Utils.ensureInBoundsOfField(coptersPos.plus(moveToBmpsVector).plus(coverVector));
      res.add(targetPos);

      double rotateAngle = Math.PI / 9;
      Point pLeft = Utils.ensureInBoundsOfField(coptersPos.plus(moveToBmpsVector.rotate(-rotateAngle)));
      Point pRight = Utils.ensureInBoundsOfField(coptersPos.plus(moveToBmpsVector.rotate(rotateAngle)));
      res.add(pLeft);
      res.add(pRight);
      return res;
   }

   @Override
   public BattleStrategyType getType() {
      return BattleStrategyType.CoptersStrategy;
   }

   @Override
   protected CoptersStrategyInfo createGroupStrategyInfo() {
      return new CoptersStrategyInfo();
   }
}
