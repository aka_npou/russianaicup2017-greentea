import java.util.ArrayList;
import java.util.List;

/**
 * User: goodg_000
 * Date: 10.11.2017
 * Time: 18:56
 */
public class CellInfo {
   private final int id;
   private final int x;
   private final int y;
   private final Point centerOfCell;

   private List<BattleUnit> myUnits = new ArrayList<>();
   private List<BattleUnit> enemyUnits = new ArrayList<>();

   List<Group> groups = new ArrayList<>();
   private long lastVisibleAtTick;

   public CellInfo(int id, int x, int y, RectSelection rectSelection) {
      this.id = id;
      this.x = x;
      this.y = y;

      double centerX = (rectSelection.getFromX() + rectSelection.getToX()) / 2;
      double centerY = (rectSelection.getFromY() + rectSelection.getToY()) / 2;
      centerOfCell = new Point(centerX, centerY);
   }

   public void clear() {
      myUnits.clear();
      enemyUnits.clear();
      groups.clear();
   }

   public void addUnit(BattleUnit u) {
      if (u.isMy()) {
         myUnits.add(u);
      }
      else {
         enemyUnits.add(u);
      }
   }

   public List<BattleUnit> getEnemyUnits() {
      return enemyUnits;
   }

   public List<BattleUnit> getMyUnits() {
      return myUnits;
   }

   public void addGroup(Group g) {
      groups.add(g);
   }

   public Point getCenterOfCell() {
      return centerOfCell;
   }

   public int getX() {
      return x;
   }

   public int getY() {
      return y;
   }

   public long getLastVisibleAtTick() {
      return lastVisibleAtTick;
   }

   public void setLastVisibleAtTick(long lastVisibleAtTick) {
      this.lastVisibleAtTick = lastVisibleAtTick;
   }

   public int getId() {
      return id;
   }
}
