import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;

/**
 * User: goodg_000
 * Date: 20.11.2017
 * Time: 20:35
 */
public final class Draw {

   private static final boolean drawEnabled = false;
   private static final boolean drawGroup = true;

   static {
      if (drawEnabled) {
         Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
               new File(GAME_LOG_FILE).delete();
            }
         });
      }
   }

   private static final String GAME_LOG_FILE = "F:/Projects/aicontests/raic2017/local-runner/draw.log";
   private static final String END_LINE = "\n";

   private static PrintWriter pw = null;
   private static LastDrawMove lastMove;

   private Draw() {
   }

   public static void startTick() {
      if (drawEnabled) {
         File file = new File(GAME_LOG_FILE);
         file.delete();
         try {
            pw = new PrintWriter(new FileOutputStream(file));
            if (Army.world != null) {
               pw.append("tick " + Army.world.getTick());
               endLine();
            }
         } catch (FileNotFoundException e) {
            e.printStackTrace();
         }
      }
   }

   public static void endTick() {
      if (drawEnabled) {
         drawMove();

         pw.flush();
         pw.close();
      }
   }

   public static void group(Group g) {
      if (drawEnabled && drawGroup) {
         pw.append("group ");
         appendPoint(g.getPos());
         appendValue(g.getGroupRadius());

         Vector2D pointsAxisVector = Vector2D.fromAngleAndLength(g.getPointsAxis(), g.getGroupRadius());
         Point s1 = g.getPos().plus(pointsAxisVector);
         Point s2 = g.getPos().plus(pointsAxisVector.negate());

         appendPoint(s1);
         appendPoint(s2);

         endLine();
      }
   }

   public static void move(LastDrawMove drawMove) {
      if (drawEnabled) {
         lastMove = drawMove;
      }
   }

   private static void drawMove() {
      if (lastMove != null) {
         pw.append("action ");
         if (lastMove.getRotationCenter() == null) {
            pw.append("move ");
            appendPoint(lastMove.getFromPos());
            appendPoint(lastMove.getToPos());
         }
         else {
            pw.append("arc ");
            appendPoint(lastMove.getFromPos());
            appendPoint(lastMove.getToPos());
            appendPoint(lastMove.getRotationCenter());
            appendValue(lastMove.getStartAngle());
            appendValue(lastMove.getRotationAngle());
         }
         endLine();
      }
   }

   private static void endLine() {
      pw.append(END_LINE);
   }

   private static void appendPoint(Point point) {
      appendValue(point.getX());
      appendValue(point.getY());
   }

   private static void appendValue(double value) {
      pw.append(("" + value).replace('.', ',')).append(" ");
   }
}
