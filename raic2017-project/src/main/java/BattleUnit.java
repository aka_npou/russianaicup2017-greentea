import model.Vehicle;
import model.VehicleType;
import model.VehicleUpdate;

import java.util.ArrayList;
import java.util.List;

/**
 * User: goodg_000
 * Date: 07.11.2017
 * Time: 21:26
 */
public class BattleUnit {
   public static double MAX_DURABILITY = 100;
   public static double RADIUS = 2;

   // static info
   private long id;
   private double radius;
   private boolean my;
   private int maxDurability;
   private double maxSpeed;
   private double visionRange;
   private double squaredVisionRange;
   private double groundAttackRange;
   private double squaredGroundAttackRange;
   private double aerialAttackRange;
   private double squaredAerialAttackRange;
   private int groundDamage;
   private int aerialDamage;
   private int groundDefence;
   private int aerialDefence;
   private int attackCooldownTicks;
   private VehicleType type;
   private boolean aerial;

   // dynamic info
   private Point pos;
   private Vector2D velocity;
   private int durability;
   private int remainingAttackCooldownTicks;
   private boolean selected;
   private int[] groups;
   private boolean hidden;

   public BattleUnit(VehicleType vehicleType) {
      this.type = vehicleType;
      durability = 100;
   }

   public BattleUnit(Vehicle vehicle, long myPlayerId) {
      // static init
      id = vehicle.getId();
      radius = vehicle.getRadius();
      my = vehicle.getPlayerId() == myPlayerId;
      maxDurability = vehicle.getMaxDurability();
      maxSpeed = vehicle.getMaxSpeed();
      visionRange = vehicle.getVisionRange();
      squaredVisionRange = vehicle.getSquaredVisionRange();
      groundAttackRange = vehicle.getGroundAttackRange();
      squaredGroundAttackRange = vehicle.getSquaredGroundAttackRange();
      aerialAttackRange = vehicle.getAerialAttackRange();
      squaredAerialAttackRange = vehicle.getSquaredAerialAttackRange();
      groundDamage = vehicle.getGroundDamage();
      aerialDamage = vehicle.getAerialDamage();
      groundDefence = vehicle.getGroundDefence();
      aerialDefence = vehicle.getAerialDefence();
      attackCooldownTicks = vehicle.getAttackCooldownTicks();
      type = vehicle.getType();
      aerial = vehicle.isAerial();

      // dynamic init
      pos = new Point(vehicle.getX(), vehicle.getY());
      velocity = new Vector2D(0, 0);
      durability = vehicle.getDurability();
      remainingAttackCooldownTicks = vehicle.getRemainingAttackCooldownTicks();
      selected = vehicle.isSelected();
      groups = vehicle.getGroups();
   }

   public void onNewTick() {
      velocity = new Vector2D();
   }

   public void update(VehicleUpdate u) {
      Point newPos = new Point(u.getX(), u.getY());
      velocity = new Vector2D(pos, newPos);
      pos = newPos;
      durability = u.getDurability();
      remainingAttackCooldownTicks = u.getRemainingAttackCooldownTicks();
      selected = u.isSelected();
      groups = u.getGroups();
   }

   public long getId() {
      return id;
   }

   public double getRadius() {
      return radius;
   }

   public boolean isMy() {
      return my;
   }

   public int getMaxDurability() {
      return maxDurability;
   }

   public double getMaxSpeed() {
      return maxSpeed;
   }

   public double getVisionRange() {
      return visionRange;
   }

   public double getSquaredVisionRange() {
      return squaredVisionRange;
   }

   public double getGroundAttackRange() {
      return groundAttackRange;
   }

   public double getSquaredGroundAttackRange() {
      return squaredGroundAttackRange;
   }

   public double getAerialAttackRange() {
      return aerialAttackRange;
   }

   public double getSquaredAerialAttackRange() {
      return squaredAerialAttackRange;
   }

   public int getGroundDamage() {
      return groundDamage;
   }

   public int getAerialDamage() {
      return aerialDamage;
   }

   public int getGroundDefence() {
      return groundDefence;
   }

   public int getAerialDefence() {
      return aerialDefence;
   }

   public int getAttackCooldownTicks() {
      return attackCooldownTicks;
   }

   public VehicleType getType() {
      return type;
   }

   public boolean isAerial() {
      return aerial;
   }

   public Point getPos() {
      return pos;
   }

   public int getDurability() {
      return durability;
   }

   public int getRemainingAttackCooldownTicks() {
      return remainingAttackCooldownTicks;
   }

   public boolean isSelected() {
      return selected;
   }

   public int[] getGroups() {
      return groups;
   }

   public List<Group> getGroups(List<Group> groups) {
      List<Group> res = new ArrayList<>();

      for (Group g : groups) {
         for (int id : this.groups) {
            if (g.getId() == id) {
               res.add(g);
               break;
            }
         }
      }

      return res;
   }

   public boolean isRemoved() {
      return getDurability() == 0;
   }

   public boolean isWithoutGroup() {
      return groups == null || groups.length == 0;
   }

   public double getDurabilityPercent() {
      return durability / MAX_DURABILITY;
   }

   public double getLostDurability() {
      return MAX_DURABILITY - durability;
   }

   public boolean inGroup(int groupId) {
      for (int id : groups) {
         if (id == groupId) {
            return true;
         }
      }
      return false;
   }

   public Vector2D getVelocity() {
      return velocity;
   }

   public Point getPosAfterTicks(int ticksCount) {
      return pos.plus(getVelocity().multiply(ticksCount));
   }

   public List<Group> findGroupsOfUnit(List<Group> allGroups) {
      List<Group> res = new ArrayList<>();
      for (Group g : allGroups) {
         for (int id : groups) {
            if (g.getId() == id) {
               res.add(g);
               continue;
            }
         }
      }
      return res;
   }

   public boolean isHidden() {
      return hidden;
   }

   public void setHidden(boolean hidden) {
      this.hidden = hidden;
   }
}
