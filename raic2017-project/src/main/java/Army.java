import model.*;

import java.util.*;

/**
 * User: goodg_000
 * Date: 09.11.2017
 * Time: 19:35
 */
public class Army {

   public static WorldInfo world;
   public static Game game;
   private static long lastSelectTick = Integer.MIN_VALUE;

   public static long getLastSelectTick() {
      return lastSelectTick;
   }

   private int tick;
   private int nextMoveTick;

   private Battlefield battlefield;

   private List<Group> groups = new ArrayList<>();

   private List<BattleStrategy> strategies = new ArrayList<>();

   int totalActionsCount = 0;

   public void update(World world, Game game) {
      Army.game = game;
      if (Army.world == null) {
         Army.world = new WorldInfo(world);
         battlefield = new Battlefield(game.getWorldWidth(), game.getWorldHeight(), 32, groups);

         initStrategies();
         getCreateGroupsStrategy().addCreateGroupRequests(createInitialGroupRequests());
         nextMoveTick = Army.world.getTick();
      }
      else {
         Army.world.updateDynamicInfo(world);
      }
      tick = Army.world.getTick();

      for (Iterator<Group> i = groups.iterator(); i.hasNext(); ) {
         Group g = i.next();
         g.update();
         if (g.isEliminated()) {
            i.remove();
            CreateGroupsStrategy.returnGroupId(g.getId());
         }
      }
   }

   public void doMove(Move move) {
      try {
         if (world.getMyPlayer().getRemainingActionCooldownTicks() > 0) {
            return;
         }

         if (tick < nextMoveTick) {
            performStrategicMove(move, true);
            return;
         }
         nextMoveTick = tick + getTicksBetweenMovesInterval();

         groups.forEach(Group::updateBeforeMove);

         battlefield.onBeforeMove();

         performStrategicMove(move, false);
      }
      finally {
         if (move.getAction() != null) {
            totalActionsCount++;
            if (move.getAction().equals(ActionType.CLEAR_AND_SELECT)) {
               lastSelectTick = tick;
            }
         }
      }
   }

   private int[] applyStrategyCount = new int[BattleStrategyType.values().length];
   private void performStrategicMove(Move move, boolean emergencyMove) {
      for (BattleStrategy s : strategies) {
         boolean applied = s.apply(move, emergencyMove);
         if (applied) {
            applyStrategyCount[s.getType().ordinal()]++;
            break;
         }
      }
   }

   private void initStrategies() {
      strategies.add(new AvoidNuclearStrikeStrategy(game, world, groups, battlefield));   // 0
      strategies.add(new CreateGroupsStrategy(game, world, groups, battlefield));         // 1
      strategies.add(new CreateScoutsStrategy(game, world, groups, battlefield));         // 2
      strategies.add(new DoNuclearStrikeStrategy(game, world, groups, battlefield));      // 3
      strategies.add(new FactoryProductionStrategy(game, world, groups, battlefield));    // 4
      strategies.add(new JoinGroupsStrategy(game, world, groups, battlefield));           // 5
      strategies.add(new PackStrategy(game, world, groups, battlefield));                 // 6
      strategies.add(new TurnToEnemyStrategy(game, world, groups, battlefield));          // 7
      strategies.add(new JoinCollidedGroupsStrategy(game, world, groups, battlefield));   // 8
      strategies.add(new CoptersStrategy(game, world, groups, battlefield));              // 9
      strategies.add(new FightersStrategy(game, world, groups, battlefield));             // 10
      strategies.add(new FactoryDefenceStrategy(game, world, groups, battlefield));       // 11
      strategies.add(new FacilityCaptureStrategy(game, world, groups, battlefield));      // 12
      strategies.add(new KillProductionStrategy(game, world, groups, battlefield));       // 13
      strategies.add(new PotentialFieldStrategy(game, world, groups, battlefield));       // 14
   }

   private CreateGroupsStrategy getCreateGroupsStrategy() {
      return (CreateGroupsStrategy) strategies.stream().filter(
              s -> s instanceof CreateGroupsStrategy).findFirst().get();
   }

   public int getTicksBetweenMovesInterval() {
      int actionsCount = game.getBaseActionCount();
      for (Facility f : world.getFacilities()) {
         if (f.getType().equals(FacilityType.CONTROL_CENTER) && f.getOwnerPlayerId() == world.getMyPlayerId()) {
            actionsCount += game.getAdditionalActionCountPerControlCenter();
         }
      }
      int res = game.getActionDetectionInterval() / actionsCount;
      if (tick > 500 && world.getEnemyPlayer().getRemainingNuclearStrikeCooldownTicks() < 100) {
         // reserving some actions before nuclear strike
         res += 1;
      }
      return res;
   }

   private List<CreateGroupRequest> createInitialGroupRequests() {
      List<CreateGroupRequest> res = new ArrayList<>();
      for (VehicleType vehicleType : VehicleType.values()) {
         res.addAll(createInitialGroupRequestsForUnits(world.getMyUnitsOfType(vehicleType), vehicleType));
      }

      return res;
   }

   private List<CreateGroupRequest> createInitialGroupRequestsForUnits(List<BattleUnit> battleUnits,
                                                                       VehicleType vehicleType) {
      List<CreateGroupRequest> res = new ArrayList<>();
      List<VehicleType> typesOfBigGroups = null;
      if (world.getFacilities().size() > 0) {
         typesOfBigGroups = Arrays.asList(VehicleType.FIGHTER, VehicleType.HELICOPTER, VehicleType.IFV, VehicleType.TANK, VehicleType.ARRV);
      }
      else {
         typesOfBigGroups = Arrays.asList(VehicleType.FIGHTER, VehicleType.HELICOPTER, VehicleType.IFV);
      }
      //typesOfBigGroups = Arrays.asList(VehicleType.FIGHTER);
      RectSelection s = RectSelection.getSelectionForUnits(battleUnits);
      if (typesOfBigGroups.contains(vehicleType)/*Utils.isAirUnit(vehicleType)*/) {
         res.add(new CreateGroupRequest(battleUnits, vehicleType));
      }
      else {
         List<RectSelection> selections = s.splitOn4Parts();
         for (RectSelection rs : selections) {
            List<BattleUnit> unitsPart = rs.filterUnitsInSelection(battleUnits);
            res.add(new CreateGroupRequest(unitsPart, vehicleType));
         }
      }
      return res;
   }

}
