import model.*;

import java.util.*;

/**
 * User: goodg_000
 * Date: 10.11.2017
 * Time: 19:12
 */
public class Battlefield {
   private static final double COLLISION_MOVE_SCORE = -10000;
   private static final int DEFAULT_GROUPS_COUNT = 17;

   private final Game game;
   private final WorldInfo world;
   
   private final double cellSize;
   private final int xCellsCount;
   private final int yCellsCount;
   private final CellInfo[][] cells;
   private final int cellsCount;
   private FightSimulator fightSimulator;

   private final Point fieldCenter;
   private final List<Point> corners;
   private final double fieldDiagonalLength;

   private List<Group> groups;

   private Set<CellInfo> cellsWithEnemyUnits = new HashSet<>();

   private List<Facility> facilities;
   private Map<CellInfo, Double> nuclearStrikeScore = new HashMap<>();
   private int tick;
   private TerrainType[][] terrains;
   private WeatherType[][] weathers;
   private boolean aggressiveMode;

   private double[] scoutScore;

   //### findBestMove caches ###########################################################################################

   private boolean canDoNuclearStrike;
   private Player myPlayer;
   private Map<Group, Map<CellInfo, Double>> battleResults = new HashMap<>();
   private Map<Group, Map<CellInfo, Double>> battleResultsOnPath = new HashMap<>();
   private List<Group> otherGroups = new ArrayList<>();
   private List<Group> otherGroupsCanCollideWith = new ArrayList<>();
   private boolean currentScoutIsBomber;

   private boolean dangerFromFighters;
   private Point averageEnemyFightersPos;

   //###################################################################################################################

   public Battlefield(double fieldWidth, double fieldHeight, double cellSize, List<Group> groups) {
      fieldCenter = new Point(fieldWidth / 2, fieldHeight / 2);
      corners = Arrays.asList(new Point(0, 0), new Point(0, fieldHeight), new Point(fieldWidth, 0),
              new Point(fieldWidth, fieldHeight));
      fieldDiagonalLength = Math.sqrt(fieldWidth*fieldWidth + fieldHeight*fieldHeight);
      this.groups = groups;

      game = Army.game;
      world = Army.world;
      DamageMatrix damageMatrix = new DamageMatrix(
              new double[] {0, 0, game.getHelicopterGroundDamage(), game.getIfvGroundDamage(), game.getTankGroundDamage()},
              new double[] {0, game.getFighterAerialDamage(), game.getHelicopterAerialDamage(), game.getIfvAerialDamage(),
                      game.getTankAerialDamage()},
              new double[] {game.getArrvGroundDefence(), game.getFighterGroundDefence(), game.getHelicopterGroundDefence(),
                      game.getIfvGroundDefence(), game.getTankGroundDefence()},
              new double[] {game.getArrvAerialDefence(), game.getFighterAerialDefence(), game.getHelicopterAerialDefence(),
                      game.getIfvAerialDefence(), game.getTankAerialDefence()}
      );
      fightSimulator = new FightSimulator(damageMatrix);
      facilities = world.getFacilities();

      this.cellSize = cellSize;
      xCellsCount = (int) Math.round(fieldWidth / cellSize);
      yCellsCount = (int) Math.round(fieldHeight / cellSize);
      cells = new CellInfo[xCellsCount][yCellsCount];
      int id = 0;
      for (int x = 0; x < xCellsCount; ++x) {
         for (int y = 0; y < yCellsCount; ++y) {
            double fromX = x * cellSize;
            double toX = fromX + cellSize;

            double fromY = y * cellSize;
            double toY = fromY + cellSize;
            cells[x][y] = new CellInfo(id, x, y, new RectSelection(fromX, toX, fromY, toY));
            id++;
         }
      }
      cellsCount = id;
      scoutScore = new double[cellsCount];

      terrains = world.getTerrains();
      weathers = world.getWeathers();
   }

   public GroupMoveResult findBestAction(Group g) {
      prepareCachesForGroup(g);

      CellInfo bestCell = getCellOfGroup(g);
      double bestScore = calcMoveGroupToCellScore(g, bestCell);
      double startScore = bestScore;

      double maxMoveDistance = cellSize * 9;
      if (groups.size() > DEFAULT_GROUPS_COUNT) {
         maxMoveDistance *= calcGroupsCountVisionFactor();
      }
      VehicleType vehicleType = g.getVehicleType();
      if (vehicleType.equals(VehicleType.FIGHTER)) {
         maxMoveDistance *= 2;
      }
      else if (vehicleType.equals(VehicleType.HELICOPTER)) {
         maxMoveDistance *= 1.5;
      }

      for (int x = 0; x < xCellsCount; ++x) {
         for (int y = 0; y < yCellsCount; ++y) {
            CellInfo c = cells[x][y];

            if (g.isBigGroup() && getDistToBorder(c) < g.getGroupRadius()) {
               continue;
            }

            if (c.getCenterOfCell().distanceTo(g.getPos()) < maxMoveDistance) {
               double score = calcMoveGroupToCellScore(g, c);
               if (score > bestScore) {
                  bestCell = c;
                  bestScore = score;
               }
            }
         }
      }

      GroupMoveResult res = new GroupMoveResult(g);

      double currentScore = g.getMoveTarget() == null ? startScore :
              calcMoveGroupToCellScore(g, getCellForPos(g.getMoveTarget()));

      res.setTarget(bestCell.getCenterOfCell());

      Point targetWithShift = getTargetWithShift(g, bestCell);
      if (isHasCollisions(g, targetWithShift)) {
         targetWithShift = bestCell.getCenterOfCell();
      }

      res.setTargetWithShift(targetWithShift);

      double scoreDiff = bestScore - currentScore;
      if (isCollisionScore(bestScore) && isCollisionScore(currentScore)) {
         // decrease score if start and best scores has collision
         scoreDiff -= 100;
      }

      res.setScoreDiff(scoreDiff);

      return res;
   }

   private void checkIfCurrentGroupIsBomber(Group g) {
      currentScoutIsBomber = false;
      if (!g.isScout()) {
         return;
      }

      if (world.getEnemyUnits().isEmpty()) {
         return;
      }

      double maxScore = Integer.MIN_VALUE;
      Group scoutWithMaxScore = null;
      for (Group group : groups) {
         if (group.isScout()) {
            double score = calcBomberPositionScore(g, getCellOfGroup(g));
            if (score > maxScore) {
               scoutWithMaxScore = group;
               maxScore = score;
            }
         }
      }
      if (scoutWithMaxScore == g) {
         currentScoutIsBomber = true;
      }
   }

   public List<BattleUnit> getEnemiesInRange(Point pos, double range) {
      List<BattleUnit> res = new ArrayList<>();

      for (int x = 0; x < xCellsCount; ++x) {
         for (int y = 0; y < yCellsCount; ++y) {
            CellInfo c = cells[x][y];
            if (!c.getEnemyUnits().isEmpty() && c.getCenterOfCell().distanceTo(pos) < range) {
               res.addAll(c.getEnemyUnits());
            }
         }
      }

      return res;
   }

   private double calcGroupsCountVisionFactor() {
      return DEFAULT_GROUPS_COUNT / (double)groups.size();
   }

   public boolean isGroupInDanger(Group g) {
      double score = calcMoveGroupToCellScore(g, getCellForPos(g.getPos()));
      if (isCollisionScore(score)) {
         score -= COLLISION_MOVE_SCORE;
      }

      int dangerScore = -5 * g.size();
      boolean res = score < dangerScore;
      return res;
   }

   public double calcMoveGroupToPosScore(Group g, Point target) {
      double score = calcMoveGroupToCellScore(g, getCellForPos(target));
      if (isCollisionScore(score)) {
         score -= COLLISION_MOVE_SCORE;
      }

      return score;
   }

   public Point getTargetWithShift(Group g, Point target) {
      CellInfo targetCell = getCellForPos(target);
      return getTargetWithShift(g, targetCell);
   }

   private Point getTargetWithShift(Group g, CellInfo targetCell) {
      List<BattleUnit> unitsCanAttack = new ArrayList<>();
      for (BattleUnit u : targetCell.getEnemyUnits()) {
         if (isCanAttack(g.getVehicleType(), u.getType())) {
            unitsCanAttack.add(u);
         }
      }
      return getTargetWithShift(g, targetCell, unitsCanAttack);
   }

   private Point getTargetWithShift(Group g, CellInfo targetCell, List<BattleUnit> units) {
      if (units.isEmpty()) {
         return targetCell.getCenterOfCell();
      }

      Vector2D v = Utils.calcAverageVelocity(units);
      Point unitsPos = Utils.calcAveragePos(Utils.getPositionsOfUnits(units));
      double dist = g.getPos().distanceTo(unitsPos);
      double ticksToGetToPoint = dist / Utils.getSpeed(g.getVehicleType());
      Point target = Utils.ensureInBoundsOfField(unitsPos.plus(v.multiply(ticksToGetToPoint)), g.getGroupRadius());
      Vector2D v1 = new Vector2D(g.getPos(), target);
      Vector2D v2 = new Vector2D(g.getPos(), unitsPos);
      if (Math.abs(v1.angleToVector(v2)) > Math.PI / 2) {
         target = unitsPos;
      }

      return target;
   }

   private double getDistToBorder(CellInfo c) {
      Point pos = c.getCenterOfCell();
      double res = Integer.MAX_VALUE;
      res = Math.min(pos.getX(), res);
      res = Math.min(pos.getY(), res);
      res = Math.min(game.getWorldWidth() - pos.getX(), res);
      res = Math.min(game.getWorldHeight() - pos.getY(), res);

      return res;
   }

   public void prepareCachesForGroup(Group g) {
      myPlayer = world.getMyPlayer();
      canDoNuclearStrike = myPlayer.getRemainingNuclearStrikeCooldownTicks() == 0 &&
              myPlayer.getNextNuclearStrikeVehicleId() == -1;

      battleResults.clear();
      battleResultsOnPath.clear();

      otherGroups.clear();
      otherGroups.addAll(groups);
      otherGroups.remove(g);

      otherGroupsCanCollideWith.clear();
      for (Group otherGroup : otherGroups) {
         if (g.isCanCollideWithGroup(otherGroup)) {
            otherGroupsCanCollideWith.add(otherGroup);
         }
      }

      dangerFromFighters = false;
      averageEnemyFightersPos = null;

      checkIfCurrentGroupIsBomber(g);
   }

   private boolean isCollisionScore(double score) {
      return score < COLLISION_MOVE_SCORE*0.8;
   }

   public NuclearStrikeResult findBestNuclearStrike(Group g) {
      double visionRange = getGroupUnisVisionRange(g);

      Point bestAttackPoint = null;
      double bestScore = Integer.MIN_VALUE;

      for (CellInfo c : cellsWithEnemyUnits) {
         Point attackPoint = c.getCenterOfCell();
         if (attackPoint.distanceTo(g.getPos()) < visionRange + g.getGroupRadius()*0.95) {
            double score = calcNuclearStrikeScore(c);
            if (score > bestScore) {
               bestScore = score;
               bestAttackPoint = attackPoint;
            }
         }
      }

      final double minBestScore = 1 + world.getEnemyUnitsCount() * 0.05;
      if (bestScore < minBestScore) {
         return null;
      }

      BattleUnit markerUnit = null;
      double maxCandidateScore = Integer.MIN_VALUE;
      for (BattleUnit u : g.getUnits()) {

         if (isTargetInVisionDuringMove(u, bestAttackPoint)) {
            double score = u.getPos().distanceTo(bestAttackPoint) + u.getDurability();
            if (score > maxCandidateScore) {
               maxCandidateScore = score;
               markerUnit = u;
            }
         }
      }

      if (markerUnit == null) {
         return null;
      }

      List<Point> candidates = getPointsInSmallRange(bestAttackPoint);
      double bestCandidateScore = Integer.MIN_VALUE;
      for (Point p : candidates) {
         if (isTargetInVisionDuringMove(markerUnit, p)) {
            double score = calcNuclearStrikeScore(p);
            if (score > bestCandidateScore) {
               bestCandidateScore = score;
               bestAttackPoint = p;
            }
         }
      }

      return new NuclearStrikeResult(bestAttackPoint, markerUnit, bestScore);
   }

   private List<Point> getPointsInSmallRange(Point target) {
      List<Point> res = new ArrayList<>();
      res.add(target);
      double smallRange = cellSize / 4;
      res.add(target);
      res.add(new Point(target.getX() + smallRange, target.getY()));
      res.add(new Point(target.getX(), target.getY() + smallRange));
      res.add(new Point(target.getX() + smallRange, target.getY() + smallRange));
      res.add(new Point(target.getX() - smallRange, target.getY()));
      res.add(new Point(target.getX(), target.getY() - smallRange));
      res.add(new Point(target.getX() - smallRange, target.getY() - smallRange));

      return res;
   }

   public int getTicksToCapture(Facility f, boolean captureByMe) {
      final int notCaptured = -1;
      double remainingPointsToCapture = 0;
      if (captureByMe) {
         remainingPointsToCapture = game.getMaxFacilityCapturePoints() - f.getCapturePoints();
      }
      else {
         remainingPointsToCapture = Math.abs(-game.getMaxFacilityCapturePoints() - f.getCapturePoints());
      }
      if (remainingPointsToCapture == 0) {
         return notCaptured;
      }

      List<CellInfo> cellsOfFacility = getCellsOfFacility(f);
      List<BattleUnit> myGroundUnitsCaptured = Utils.filterGroundUnits(getMyUnitsInCells(cellsOfFacility));
      List<BattleUnit> enemyGroundUnitsCaptured = Utils.filterGroundUnits(getEnemyUnitsInCells(cellsOfFacility));

      int unitsDiff = myGroundUnitsCaptured.size() - enemyGroundUnitsCaptured.size();
      if (!captureByMe) {
         unitsDiff *= -1;
      }
      if (unitsDiff <= 0) {
         return notCaptured;
      }

      double cp = game.getFacilityCapturePointsPerVehiclePerTick();
      int res = (int) (remainingPointsToCapture / (unitsDiff*cp));

      return res;
   }

   private List<CellInfo> getCellsOfFacility(Facility f) {
      List<CellInfo> res = new ArrayList<>();

      double d = cellSize / 2;
      double left = f.getLeft();
      double top = f.getTop();
      res.add(getCellForPos(new Point(left + d, top + d)));
      res.add(getCellForPos(new Point(left + 3*d, top + d)));
      res.add(getCellForPos(new Point(left + d, top + 3*d)));
      res.add(getCellForPos(new Point(left + 3*d, top + 3*d)));

      return res;
   }

   private List<BattleUnit> getMyUnitsInCells(List<CellInfo> cells) {
      List<BattleUnit> res = new ArrayList<>();
      for (CellInfo c : cells) {
         res.addAll(c.getMyUnits());
      }
      return res;
   }

   private List<BattleUnit> getEnemyUnitsInCells(List<CellInfo> cells) {
      List<BattleUnit> res = new ArrayList<>();
      for (CellInfo c : cells) {
         res.addAll(c.getEnemyUnits());
      }
      return res;
   }

   private boolean isTargetInVisionDuringMove(BattleUnit u, Point target) {
      Vector2D v = u.getVelocity();
      if (v.isZero()) {
         return u.getPos().distanceTo(target) < getUnitVisionRange(u, u.getPos());
      }

      int stepsPerTick = 2;
      int totalTicks = game.getTacticalNuclearStrikeDelay();
      Point pos = u.getPos();
      double minDiff = Integer.MAX_VALUE;
      for (int i = 0; i < totalTicks*stepsPerTick; ++i) {
         double diff = getUnitVisionRange(u, pos) - pos.distanceTo(target);
         if (diff < minDiff) {
            minDiff = diff;
         }
         if (diff < 0) {
            return false;
         }
         pos = pos.plus(v.setLength(getUnitSpeed(u, pos) / stepsPerTick));
      }
      return true;
   }

   private double calcNuclearStrikeScore(CellInfo target) {
      Double res = nuclearStrikeScore.get(target);
      if (res != null) {
         return res;
      }

      Point targetPos = target.getCenterOfCell();

      double score = 0;
      if (!target.getEnemyUnits().isEmpty()) {
         score = calcNuclearStrikeScore(targetPos);
      }

      nuclearStrikeScore.put(target, score);
      return score;
   }

   private double calcNuclearStrikeScore(Point targetPos) {
      double score = 0;

      final double explosionRange = game.getTacticalNuclearStrikeRadius();
      final double damageInCenter = game.getMaxTacticalNuclearStrikeDamage();

      for (BattleUnit u : world.getAllUnits()) {
         Point pos = u.getPos();
         if (u.isMy()) {
            pos = u.getPosAfterTicks(game.getTacticalNuclearStrikeDelay());
         }
         else if (u.isHidden()) {
            continue;
         }

         double dist = pos.distanceTo(targetPos);
         if (dist < explosionRange) {
            double maxDamage = Utils.linearInterpolate(dist, 0, explosionRange, damageInCenter, 0);
            double s = maxDamage*0.01;
            if (s > u.getDurabilityPercent()) {
               s = 1;
            }

            if (u.isMy()) {
               s *= -1;
            }
            else {
               if (u.getType().equals(VehicleType.TANK)) {
                  s *= 1.01;
               }
               else if (u.getType().equals(VehicleType.FIGHTER)) {
                  s *= 0.65;
               }
               else if (u.getType().equals(VehicleType.HELICOPTER)) {
                  s *= 0.75;
               }
               else if (u.getType().equals(VehicleType.ARRV)) {
                  s *= 0.1;
               }
            }

            score += s;
         }
      }
      return score;
   }

   private List<CellInfo> getNearCellsExtended(CellInfo c) {
      List<CellInfo> res = new ArrayList<>();
      int x = c.getX();
      int y = c.getY();
      if (x < xCellsCount - 1) {
         res.add(cells[x + 1][y]);
         if (y < yCellsCount - 1) {
            res.add(cells[x + 1][y + 1]);
         }
         if (y > 0) {
            res.add(cells[x + 1][y - 1]);
         }
      }
      if (y < yCellsCount - 1) {
         res.add(cells[x][y + 1]);
      }
      if (x > 0) {
         res.add(cells[x - 1][y]);
         if (y < yCellsCount - 1) {
            res.add(cells[x - 1][y + 1]);
         }
         if (y > 0) {
            res.add(cells[x - 1][y - 1]);
         }
      }
      if (y > 0) {
         res.add(cells[x][y - 1]);
      }

      return res;
   }

   public boolean isCanSafelyMoveToPoint(Group g, Point target) {
      prepareCachesForGroup(g);
      return !isHasCollisions(g, target) && !isGroupInDanger(g) && calcBattleScoreOnPath(g, target) >= 0;
   }

   public boolean isCanSafelyMoveToPoint2(Group g, Point target) {
      prepareCachesForGroup(g);
      return !isHasCollisions(g, target) && !isGroupInDanger(g) && calcMoveGroupToPosScore(g, target) >= 0;
   }

   public boolean isHasCollisions(Group g, Point target) {
      return isHasCollisions(g, null, target);
   }

   public boolean isHasCollisions(Group g, Group excludedGroup, Point target) {
      Point startPos = g.getPos();
      double distToTarget = g.getPos().distanceTo(target);
      Vector2D moveVector = new Vector2D(startPos, target);
      Vector2D moveDirection = new Vector2D(startPos, target).unit();
      if (moveVector.isZero()) {
         return false;
      }

      double groupSpeed = Utils.getSpeed(g.getVehicleType());
      double currDist = 1;
      int i = 0;
      final int simulationStepInc = 30;
      int step = simulationStepInc / 2;

      List<Group> testGroups = new ArrayList<>();
      for (Group testGroup : otherGroupsCanCollideWith) {
         if (testGroup == excludedGroup) {
            continue;
         }

         double distToTestGroup = g.getPos().distanceTo(testGroup.getPos());
         double collisionDist = g.getGroupRadius() + testGroup.getGroupRadius() + BattleUnit.RADIUS*2;
         double testGroupMoveDist = 0;
         if (testGroup.getMoveTargetWithShift() != null) {
            testGroupMoveDist = testGroup.getPos().distanceTo(testGroup.getMoveTargetWithShift());
         }
         if (distToTestGroup < distToTarget + testGroupMoveDist + collisionDist) {
            testGroups.add(testGroup);
         }
      }

      if (testGroups.size() > 0) {
         boolean finish = false;
         while (!finish) {
            double length = groupSpeed * step;
            if (length > distToTarget) {
               length = distToTarget;
               finish = true;
            }
            Point currPos = startPos.plus(moveDirection.clone().setLength(length));

            for (Group testGroup : testGroups) {
               double testGroupSpeed = Utils.getSpeed(testGroup.getVehicleType());
               Point moveTargetWithShift = testGroup.getMoveTargetWithShift();
               Vector2D testGroupMoveVector = new Vector2D(testGroup.getPos(),
                       moveTargetWithShift != null ? moveTargetWithShift : testGroup.getPos());

               Point testGroupPos = testGroup.getPos();
               if (!testGroupMoveVector.isZero()) {
                  double moveDist = Math.min(testGroupSpeed * step, testGroupMoveVector.length());
                  testGroupPos = testGroupPos.plus(testGroupMoveVector.clone().setLength(moveDist));
               }

               double collisionDist = g.getGroupRadius() + testGroup.getGroupRadius() + BattleUnit.RADIUS*2;
               if (currPos.distanceTo(testGroupPos) < collisionDist) {
                  return true;
               }
            }

            if (currDist == distToTarget) {
               break;
            }
            currDist += groupSpeed;
            if (currDist > distToTarget) {
               currDist = distToTarget;
            }

            i++;
            step += simulationStepInc;
         }
      }

      Segment moveSegment = new Segment(startPos, target);
      for (Facility f : facilities) {
         if (!f.getType().equals(FacilityType.VEHICLE_FACTORY)) {
            continue;
         }

         List<BattleUnit> producedUnits = getProducedUnits(f);
         if (producedUnits.isEmpty()) {
            continue;
         }

         boolean hasGroundUnis = !Utils.filterGroundUnits(producedUnits).isEmpty();
         boolean hasAirUnits = !Utils.filterAirUnits(producedUnits).isEmpty();

         if ((g.isHasGroundUnits() && hasGroundUnis) || (g.isHasAirUnits() && hasAirUnits)) {
            RectSelection selection = RectSelection.getSelectionForUnits(producedUnits);
            double collisionDist = g.getGroupRadius() + BattleUnit.RADIUS*2;
            for (Segment s : selection.getBounds()) {
               if (moveSegment.findNearestDistToSegment(s) < collisionDist) {
                  return true;
               }
            }
         }
      }

      return false;
   }

   public List<BattleUnit> getProducedUnits(Facility f) {
      RectSelection selection = RectSelection.getFacilitySelection(f);
      return selection.filterUnitsInSelection(world.getMyFreeUnits());
   }

   public boolean isCanAttack(VehicleType fromType, VehicleType toType) {
      return fightSimulator.getDamageMatrix().getDamage(fromType, toType) > 0;
   }

   public boolean isCanAttackEachOther(VehicleType fromType, VehicleType toType) {
      return isCanAttack(fromType, toType) && isCanAttack(fromType, toType);
   }

   public double calcBattleScoreOnPath(Group g, Point target) {
      Vector2D moveVector = new Vector2D(g.getPos(), target);
      if (moveVector.length() <= cellSize) {
         return 0;
      }
      double length = moveVector.length() - cellSize;
      moveVector.setLength(length);
      final double closeDist = g.getGroupRadius() + Utils.getAttackRange(g.getVehicleType());

      Segment moveSegment = new Segment(g.getPos().plus(moveVector.clone().setLength(cellSize)),
              g.getPos().plus(moveVector));

      double res = 0;
      for (CellInfo enemyCell : cellsWithEnemyUnits) {
         Point pos = enemyCell.getCenterOfCell();
         if (g.getPos().distanceTo(pos) > length + closeDist) {
            continue;
         }

         double dist = moveSegment.distanceTo(pos);
         if (dist < closeDist) {
            double kd = calcUnitsKilledMinusLostScore(g, enemyCell, true);
            res += kd;
         }
      }
      return res;
   }

   private CellInfo getCellOfGroup(Group g) {
      return getCellForPos(g.getPos());
   }

   private double calcMoveGroupToCellScore(Group g, CellInfo c) {
      double score = 0;

      if (!g.isScout()) {
         if (!aggressiveMode) {
            score += calcFieldPositionScore(c);
            if (g.getVehicleType().equals(VehicleType.ARRV)) {
               score += calcHealTeammatesScore(g, c);
            }
            if (g.getTotalLostDurability() > 0 && g.isHasAirUnits()) {
               score += calcGoToHealersScore(g, c);
            }
            if (dangerFromFighters) {
               score += calcSaveHelicoptersScore(g, c);
            }
            score += calcEnvironmentScore(g, c);
         }

         score += calcDistanceToMyGroupsScore(g, c);
         score += calcBattleScore(g, c);
         if (g.isHasGroundUnits()) {
            score += calcCaptureFacilitiesScore(g, c);
         }
         else {
            score += 0.2*calcCaptureFacilitiesScore(g, c);
         }

         if (g.getVehicleType().equals(VehicleType.FIGHTER)
                 && world.getEnemyUnitsOfType(VehicleType.HELICOPTER).size() == 0) {
            score += calcScoutScore(c);

//            if (tick < 1500) {
//               score += calcGoToMirrorGroupsScore(c);
//            }
         }
      }
      else {
         //score += calcFieldPositionScore(c);
         score += calcDistanceToMyGroupsScore(g, c);
         if (!currentScoutIsBomber) {
            score += calcScoutScore(c);
            score += calcDistFromOtherScoutsScore(g, c);
         }

         double battleScore = calcScoutBattleScore(g, c);
         score += battleScore;
         if (currentScoutIsBomber && battleScore >= 0) {
            score += calcBomberPositionScore(g, c);
         }
      }

      return score;
   }

   private double calcEnvironmentScore(Group g, CellInfo c) {

      double score = 0;
      Vector2D moveVector = new Vector2D(g.getPos(), c.getCenterOfCell());
      final double length = moveVector.length();
      if (length < cellSize) {
         return score;
      }

      final double step = cellSize;

      double currLength = 1;
      while (true) {
         CellInfo cell = getCellForPos(g.getPos().plus(moveVector.setLength(currLength)));
         if (g.isHasAirUnits()) {
            WeatherType wt = getWeatherType(cell);
            score += getWeatherTypePenalty(wt);
         }
         if (g.isHasGroundUnits()) {
            TerrainType tt = getTerrainType(cell);
            score += getTerrainTypePenalty(tt);
         }

         if (currLength == length) {
            break;
         }

         currLength += step;
         if (currLength > length) {
            currLength = length;
         }
      }

      score *= (g.size() / 50.0);

      return score;
   }

   public double getWeatherTypePenalty(WeatherType weatherType) {
      double score = 0;
      if (weatherType.equals(WeatherType.CLOUD)) {
         score = -1;
      }
      if (weatherType.equals(WeatherType.RAIN)) {
         score = -2;
      }

      return score;
   }

   public double getTerrainTypePenalty(TerrainType terrainType) {
      double score = 0;
      if (terrainType.equals(TerrainType.SWAMP)) {
         score = -1;
      }
      if (terrainType.equals(TerrainType.FOREST)) {
         score = -2;
      }

      return score;
   }

   private double calcCaptureFacilitiesScore(Group g, CellInfo c) {
      double score = 0;

      Point pos = c.getCenterOfCell();
      double baseScore = 2;
      Facility nearestFreeFacility = null;
      double minDist = Integer.MAX_VALUE;
      for (Facility f : facilities) {
         if (f.getOwnerPlayerId() != myPlayer.getId()) {
            double dist = pos.distanceTo(getFacilityCenter(f));
            if (dist < minDist) {
               minDist = dist;
               nearestFreeFacility = f;
            }
         }
      }
      if (nearestFreeFacility != null) {
         double dist = pos.distanceTo(getFacilityCenter(nearestFreeFacility));
         double s = Utils.linearInterpolate(dist, 0, fieldDiagonalLength, baseScore, 0);
         score += s*s*s*s;

         if (isFacilityCell(nearestFreeFacility, c)) {
            double facilityScore = 5;
            if (nearestFreeFacility.getType().equals(FacilityType.VEHICLE_FACTORY)) {
               facilityScore *= 6;
            }
            score += facilityScore;
         }
      }

      score *= (g.size() / 100.0);
      return score;
   }

   public Point getFacilityCenter(Facility f) {
      double x = f.getLeft() + game.getFacilityWidth() / 2;
      double y = f.getTop() + game.getFacilityHeight() / 2;
      return new Point(x, y);
   }

   private boolean isFacilityCell(Facility f, CellInfo c) {
      Point pos = c.getCenterOfCell();
      return f.getLeft() < pos.getX() && (f.getLeft() + game.getFacilityWidth()) > pos.getX() &&
             f.getTop() < pos.getY() && (f.getTop() + game.getFacilityHeight()) > pos.getY();
   }

   private double getGroupUnisVisionRange(Group g) {
      double baseVisionRange = g.getUnits().get(0).getVisionRange();
      return baseVisionRange * getVisionFactor(g.getVehicleType(), g.getPos());
   }

   private double getUnitVisionRange(BattleUnit u, Point pos) {
      double baseVisionRange = u.getVisionRange();
      return baseVisionRange * getVisionFactor(u.getType(), pos);
   }

   private double getUnitTypeVisionRange(VehicleType unitType, Point pos) {
      double baseVisionRange = Utils.getVisionRange(unitType);
      return baseVisionRange * getVisionFactor(unitType, pos);
   }

   private double getVisionFactor(VehicleType vehicleType, Point pos) {
      double res;
      CellInfo c = getCellForPos(pos);
      if (Utils.isAirUnit(vehicleType)) {
         res = Utils.getWeatherVisionFactor(getWeatherType(c));
      }
      else {
         res = Utils.getTerrainVisionFactor(getTerrainType(c));
      }
      return res;
   }

   private double getUnitSpeed(BattleUnit u, Point pos) {
      double baseSpeed = u.getMaxSpeed();
      return baseSpeed * getSpeedFactor(u.getType(), pos);
   }

   private double getSpeedFactor(VehicleType vehicleType, Point pos) {
      double res;
      CellInfo c = getCellForPos(pos);
      if (Utils.isAirUnit(vehicleType)) {
         res = Utils.getWeatherSpeedFactor(getWeatherType(c));
      }
      else {
         res = Utils.getTerrainSpeedFactor(getTerrainType(c));
      }
      return res;
   }

   private WeatherType getWeatherType(CellInfo c) {
      return weathers[c.getX()][c.getY()];
   }

   private TerrainType getTerrainType(CellInfo c) {
      return terrains[c.getX()][c.getY()];
   }

   private double calcSaveHelicoptersScore(Group hg, CellInfo c) {
      double score = 0;

      if (averageEnemyFightersPos == null) {
         averageEnemyFightersPos = Utils.calcAveragePosOfUnits(world.getEnemyUnitsOfType(VehicleType.FIGHTER));
      }

      for (Group g : otherGroups) {
         if (g.getVehicleType().equals(VehicleType.IFV) || g.getVehicleType().equals(VehicleType.FIGHTER)) {
            Point pos = g.getPos().plus(new Vector2D(averageEnemyFightersPos, g.getPos()).setLength(
                    hg.getGroupRadius()*2));
            if (calcBattleScoreOnPath(hg, pos) < 0) {
               continue;
            }

            double dist = c.getCenterOfCell().distanceTo(pos);
            UnitsCounts unitsCounts = g.getUnitsCounts();

            double baseScore = unitsCounts.get(VehicleType.IFV);
            baseScore += unitsCounts.get(VehicleType.FIGHTER);
            double s = Utils.linearInterpolate(dist, 0, fieldDiagonalLength, baseScore, 0);
            score += s;
         }
      }

      return score;
   }

   private double calcFieldPositionScore(CellInfo c) {
      double score = 0;
      double distToCenter = c.getCenterOfCell().distanceTo(fieldCenter);

      final double farFromCenterFactor = 3;
      score -= Utils.linearInterpolate(distToCenter, 0, fieldDiagonalLength / 2,
              0, farFromCenterFactor);

      Point nearestCorner = c.getCenterOfCell().findNearestPoint(corners);
      final double nearToCornerFactor = 3;
      final double nearToCornerDangerDist = fieldDiagonalLength * 0.3;
      double distToCorner = c.getCenterOfCell().distanceTo(nearestCorner);
      if (distToCorner < nearToCornerDangerDist) {
         score -= Utils.linearInterpolate(distToCorner, 0, nearToCornerDangerDist, nearToCornerFactor,
                 0);
      }

      return score;
   }

   private double calcGoToHealersScore(Group g, CellInfo c) {
      double score = 0;
      if (g.getVehicleType().equals(VehicleType.ARRV)) {
         return score;
      }

      double totalLostDurability = g.getTotalLostDurability();
      if (totalLostDurability == 0) {
         return score;
      }

      for (Group otherGroup : otherGroups) {
         double healears = otherGroup.getUnitsCounts().get(VehicleType.ARRV);
         if (healears == 0) {
            continue;
         }

         if (calcBattleScoreOnPath(g, otherGroup.getPos()) < 0) {
            continue;
         }

         Point targetWithShift = getTargetWithShift(g, getCellOfGroup(otherGroup),
                 otherGroup.getUnits());

         double dist = targetWithShift.distanceTo(c.getCenterOfCell());
         double baseScore = totalLostDurability * healears * 0.003;
         double closeRangeEndScore = baseScore * 0.2;
         double closeRange = 75;
         if (dist < closeRange) {
            score += Utils.linearInterpolate(dist, 0, closeRange, baseScore, closeRangeEndScore);
         }
         else {
            score += Utils.linearInterpolate(dist, closeRange, fieldDiagonalLength, closeRangeEndScore, 0);
         }
      }
      return score;
   }

   private double calcHealTeammatesScore(Group hg, CellInfo c) {
      double score = 0;
      for (Group g : otherGroups) {
         double totalLostDurability = g.getTotalLostDurability();
         if (totalLostDurability == 0) {
            continue;
         }

         if (!g.isHasAirUnits()) {
            continue;
         }

         if (calcBattleScoreOnPath(hg, g.getPos()) < 0) {
            continue;
         }

         double dist = c.getCenterOfCell().distanceTo(g.getAveragePosOfDamagedUnits());

         double baseScore = totalLostDurability * 0.05;
         double closeRangeEndScore = baseScore * 0.2;
         double closeRange = 75;
         if (dist < closeRange) {
            score += Utils.linearInterpolate(dist, 0, closeRange, baseScore, closeRangeEndScore);
         }
         else {
            score += Utils.linearInterpolate(dist, closeRange, fieldDiagonalLength, closeRangeEndScore, 0);
         }
      }
      return score;
   }

   private double calcDistanceToMyGroupsScore(Group g, CellInfo c) {
      double score = 0;

      Point targetPos = c.getCenterOfCell();
      final double farFromOtherGroupsDist = 60;
      final double farFromOtherGroupsFactor = 10;

      if (isHasCollisions(g, c.getCenterOfCell())) {
         return COLLISION_MOVE_SCORE;
      }

      if (g.getVehicleType().equals(VehicleType.ARRV)) {
         return score;
      }

      for (Group other : otherGroups) {
         if (other.getVehicleType().equals(VehicleType.ARRV)) {
            continue;
         }

         double dist = targetPos.distanceTo(other.getPos());
         if (g.isCanCollideWithGroup(other)) {
            if (dist < farFromOtherGroupsDist) {
               score -= (dist / farFromOtherGroupsDist) * farFromOtherGroupsFactor;
            }
         }

         if (game.isFogOfWarEnabled() && g.getVehicleType().equals(VehicleType.HELICOPTER)
                 && other.getVehicleType().equals(VehicleType.IFV) && other.size() > 20) {
            if (dist > other.getGroupRadius()) {
               double s = Utils.linearInterpolate(dist, other.getGroupRadius(), farFromOtherGroupsDist,
                       1, 0);
               score += s;
            }
         }
      }

      return score;
   }

   public List<CellInfo> calcDangerCells(Group g) {
      List<CellInfo> res = new ArrayList<>();
      for (CellInfo enemyCell : cellsWithEnemyUnits) {
         if (calcUnitsKilledMinusLostScore(g, enemyCell, false) < 0) {
            res.add(enemyCell);
         }
      }
      return res;
   }

   private double calcScoutBattleScore(Group g, CellInfo c) {
      double score = 0;

      Segment moveSegment = null;
      double length = 0;
      Point moveTargetPos = c.getCenterOfCell();
      Vector2D moveVector = new Vector2D(g.getPos(), moveTargetPos);
      if (moveVector.length() > cellSize) {
         length = moveVector.length() - cellSize;
         moveVector.setLength(length);
         moveSegment = new Segment(g.getPos().plus(moveVector.clone().setLength(cellSize)), g.getPos().plus(moveVector));
      }

      for (CellInfo enemyCell : cellsWithEnemyUnits) {
         boolean dangerCell = false;
         double maxCloseRange = 0;

         for (BattleUnit enemyUnit : enemyCell.getEnemyUnits()) {
            if (isCanAttack(enemyUnit.getType(), g.getVehicleType())) {
               dangerCell = true;
               double closeRange = 0;
               if (enemyUnit.getType().equals(VehicleType.FIGHTER)) {
                  closeRange = 130;
               }
               else if (enemyUnit.getType().equals(VehicleType.HELICOPTER)) {
                  closeRange = 110;
               }
               else if (enemyUnit.getType().equals(VehicleType.IFV)) {
                  closeRange = 70;
               }
               if (closeRange > maxCloseRange) {
                  maxCloseRange = closeRange;
               }
            }
         }

         Point enemyCellPos = enemyCell.getCenterOfCell();
         double dist = moveTargetPos.distanceTo(enemyCellPos);
         if (dist > maxCloseRange) {
            if (moveSegment != null) {
               if (g.getPos().distanceTo(enemyCellPos) > length + maxCloseRange) {
                  continue;
               }

               dist = moveSegment.distanceTo(enemyCellPos);
               if (dist > maxCloseRange) {
                  continue;
               }
            }
            else {
               continue;
            }
         }

         if (dangerCell) {
            double s = Utils.linearInterpolate(dist, 0, maxCloseRange, 15, 0);
            score += -s*s;
         }

      }

      return score;
   }

   private double calcBomberPositionScore(Group g, CellInfo c) {

      double res = 0;

      double maxScore = Integer.MIN_VALUE;
      CellInfo bestAttackCell = null;
      for (int x = 0; x < xCellsCount; ++x) {
         for (int y = 0; y < yCellsCount; ++y) {
            CellInfo attackCell = cells[x][y];
            double score = calcNuclearStrikeScore(attackCell);
            if (score > maxScore) {
               maxScore = score;
               bestAttackCell = attackCell;
            }
         }
      }

      double dist = bestAttackCell.getCenterOfCell().distanceTo(c.getCenterOfCell());
      if (dist < game.getTacticalNuclearStrikeRadius()) {
         res -= 20;
      }
      else if (dist < game.getTacticalNuclearStrikeRadius() + 32) {
         res += 80;
      }
      else {
         res += Utils.linearInterpolate(dist, 0, fieldDiagonalLength, 60, 0);
      }

      return res;
   }

   private double calcBattleScore(Group g, CellInfo c) {
      double score = 0;
      double goToEnemyRange = 250 - tick*0.003;
      double closeRange = 75;
      if (g.getVehicleType().equals(VehicleType.FIGHTER)) {
         goToEnemyRange = 80 + g.getGroupRadius();
         closeRange = 50 + g.getGroupRadius();
      }
      if (aggressiveMode) {
         goToEnemyRange = 100;
         closeRange = 50;
      }

      score += calcBattleScoreOnPath(g, c.getCenterOfCell());

      for (CellInfo enemyCell : cellsWithEnemyUnits) {
         double dist = c.getCenterOfCell().distanceTo(enemyCell.getCenterOfCell());

         if (dist > goToEnemyRange) {
            double baseScore = 3;
            double s = Utils.linearInterpolate(dist, goToEnemyRange, fieldDiagonalLength, 0, -baseScore);
            score += s;
         }
         else {
            double baseScore = calcUnitsKilledMinusLostScore(g, enemyCell, false);

            double s = 0;
            double closeRangeEndScore = baseScore * 0.02;
            if (dist < closeRange) {
               s += Utils.linearInterpolate(dist, 0, closeRange, baseScore, closeRangeEndScore);
            }
            else {
               s += Utils.linearInterpolate(dist, closeRange, fieldDiagonalLength, closeRangeEndScore, 0);
            }
            score += s;
         }

      }

      double dist = g.getPos().distanceTo(c.getCenterOfCell());
      double killRange = cellSize * 4;
      if (score > 0 && dist < killRange) {
         score *= Utils.linearInterpolate(dist, 0, killRange, 20, 1);
      }
      //score *= Utils.linearInterpolate(dist, 0, fieldDiagonalLength, 1, 0.4);

      return score;
   }

   private double calcUnitsKilledMinusLostScore(Group g, CellInfo c, boolean onPath) {
      Map<Group, Map<CellInfo, Double>> cache = onPath ? battleResultsOnPath : battleResults;
      Map<CellInfo, Double> m1 = battleResults.get(g);
      if (m1 == null) {
         m1 = new HashMap<>();
         cache.put(g, m1);
      }
      Double res = m1.get(c);
      if (res != null) {
         return res;
      }

      UnitsCounts unitsOfGroup = g.getUnitsCounts();
      UnitsCounts myUnits = unitsOfGroup.copy();
      UnitsCounts myUnitsWithoutCurrentGroup = new UnitsCounts();
      Point targetPos = c.getCenterOfCell();
      final double fightDist = g.getGroupRadius()*2;

      List<CellInfo> cells = getNearCellsExtended(c);
      cells.add(c);

      UnitsCounts enemyUnits = new UnitsCounts();
      for (CellInfo testCell : cells) {
         for (BattleUnit u : testCell.getMyUnits()) {
            if (!u.inGroup(g.getId())) {
               double dist = targetPos.distanceTo(u.getPos());
               if (dist < fightDist) {
                  myUnits.add(u.getType(), u.getDurabilityPercent());
                  myUnitsWithoutCurrentGroup.add(u.getType(), u.getDurabilityPercent());
               }
            }
         }
         for (BattleUnit u : testCell.getEnemyUnits()) {
            double dist = targetPos.distanceTo(u.getPos());
            if (dist < fightDist) {
               enemyUnits.add(u.getType(), u.getDurabilityPercent());
            }
         }
      }

      Facility f = getFacilityInCell(c);
      VehicleType myProduced = null;
      VehicleType enemyProduced = null;
      if (f != null) {
         if (f.getOwnerPlayerId() == myPlayer.getId()) {
            myProduced = f.getVehicleType();
         }
         else if (f.getOwnerPlayerId() == world.getEnemyPlayer().getId()) {
            enemyProduced = f.getVehicleType();
         }
      }

      double resWithoutCurrent;
      boolean canSkipFight = onPath && g.isHasAirUnits() && !g.isHasGroundUnits();
      if (canSkipFight) {
         resWithoutCurrent = fightSimulator.fightSingleRound(myUnitsWithoutCurrentGroup, enemyUnits);
         res = fightSimulator.fightSingleRound(myUnits, enemyUnits);
      }
      else {
         resWithoutCurrent = fightSimulator.fight(myUnitsWithoutCurrentGroup, enemyUnits,
                 myProduced, enemyProduced).getKilledMinusLost();
         res = fightSimulator.fight(myUnits, enemyUnits, myProduced, enemyProduced).getKilledMinusLost();
      }

      if (res == resWithoutCurrent) {
         if (canSkipFight) {
            res = fightSimulator.fightSingleRound(unitsOfGroup, enemyUnits);
         }
         else {
            res = fightSimulator.fight(unitsOfGroup, enemyUnits, myProduced, enemyProduced).getKilledMinusLost();
         }
      }

      if (g.getVehicleType().equals(VehicleType.HELICOPTER) && res < 0 && enemyUnits.get(VehicleType.FIGHTER) > 0) {
         double dist = g.getPos().distanceTo(c.getCenterOfCell());
         final double helicopterDangerDist = 400;
         if (dist < helicopterDangerDist) {
            dangerFromFighters = true;
         }
      }

      if (!aggressiveMode) {
         if (res < 0) {
            // units saving has more priority then attack
            res *= 10;
         }
      }
      else {
         if (res > 0) {
            res *= 10;
         }
      }

      m1.put(c, res);
      return res;
   }

   public boolean isCanDefend(List<BattleUnit> defenders, VehicleType producedUnitType, List<BattleUnit> attackers) {
      UnitsCounts u1 = new UnitsCounts(defenders);
      UnitsCounts u2 = new UnitsCounts(attackers);
      return fightSimulator.fight(u1, u2, producedUnitType, null).getU1().isNotEmpty();
   }

   public long calcDefendTime(List<BattleUnit> defenders, VehicleType producedUnitType, List<BattleUnit> attackers) {
      UnitsCounts u1 = new UnitsCounts(defenders);
      UnitsCounts u2 = new UnitsCounts(attackers);
      return fightSimulator.fight(u1, u2, producedUnitType, null).getEndRound() * 60;
   }

   public Integer getMaxTicksToDefend(List<BattleUnit> defenders, List<BattleUnit> attackers) {
      UnitsCounts u1 = new UnitsCounts(defenders);
      UnitsCounts u2 = new UnitsCounts(attackers);

      FightResult fightResult = fightSimulator.fight(u1, u2);
      boolean u1Killed = fightResult.getU1().isEmpty() && fightResult.getU2().isNotEmpty();
      return u1Killed ? fightResult.getEndRound() * 60 : null;
   }

   private void addUnits(List<BattleUnit> units) {
      for (BattleUnit u : units) {
         addUnit(u);
      }
   }

   private void addVision(List<BattleUnit> myUnits) {
      if (game.isFogOfWarEnabled()) {
         for (BattleUnit u : myUnits) {
            double visionRange = getUnitVisionRange(u, u.getPos());

            for (int x = 0; x < xCellsCount; ++x) {
               for (int y = 0; y < yCellsCount; ++y) {
                  CellInfo c = cells[x][y];

                  if (c.getLastVisibleAtTick() != tick) {
                     double dist = u.getPos().distanceTo(c.getCenterOfCell());
                     if (dist < visionRange) {
                        c.setLastVisibleAtTick(tick);
                     }
                  }
               }
            }
         }
      }
      else {
         for (int x = 0; x < xCellsCount; ++x) {
            for (int y = 0; y < yCellsCount; ++y) {
               CellInfo c = cells[x][y];
               c.setLastVisibleAtTick(tick);
            }
         }
      }

   }

   private double calcGoToMirrorGroupsScore(CellInfo c) {
      double score = 0;

      for (Group g : groups) {
         Point mirrorPos = getMirrorPos(g.getPos());
         double dist = mirrorPos.distanceTo(c.getCenterOfCell());
         if (g.getVehicleType().equals(VehicleType.IFV)) {
            double s = Utils.linearInterpolate(dist, 0, fieldDiagonalLength, 10, 0);
            score += s*s;
         }
         else if (g.getVehicleType().equals(VehicleType.ARRV) || g.getVehicleType().equals(VehicleType.TANK)) {
            double s = Utils.linearInterpolate(dist, 0, fieldDiagonalLength, 6, 0);
            score += s*s;
         }
      }

      return score;
   }

   private Point getMirrorPos(Point pos) {
      return new Point(game.getWorldWidth() - pos.getX(), game.getWorldHeight() - pos.getY());
   }

   private double calcScoutScore(CellInfo c) {
      if (!game.isFogOfWarEnabled()) {
         return 0;
      }

      double v = scoutScore[c.getId()];
      if (v != -1) {
         return v;
      }

      final double visionRange = getUnitTypeVisionRange(VehicleType.FIGHTER, c.getCenterOfCell());

      double res = 0;
      for (int x = 0; x < xCellsCount; ++x) {
         for (int y = 0; y < yCellsCount; ++y) {
            CellInfo cell = cells[x][y];
            double dist = c.getCenterOfCell().distanceTo(cell.getCenterOfCell());
            if (dist < visionRange) {
               double score = 0.0002 * (tick - cell.getLastVisibleAtTick());
               Facility f = getFacilityInCell(cell);
               if (f != null) {
                  if (f.getType().equals(FacilityType.VEHICLE_FACTORY)) {
                     score *= 10;
                  }
                  else {
                     score *= 2;
                  }
               }

               for (BattleUnit u : cell.getEnemyUnits()) {
                  if (u.isHidden()) {
                     score += 0.1;
                     if (u.getType().equals(VehicleType.HELICOPTER)) {
                        score += 0.2;
                     }
                  }
               }

               res += score;
            }
         }
      }
      scoutScore[c.getId()] = res;
      return res;
   }

   public double findMinGroupSpeedOnPath(Group g, Point moveTarget) {
      Set<VehicleType> vehicleTypes = new HashSet<>();
      for (BattleUnit u : g.getUnits()) {
         vehicleTypes.add(u.getType());
      }

      double minSpeed = Integer.MAX_VALUE;
      final double cellRadius = Utils.hypot(cellSize, cellSize) / 2;

      Segment moveSegment = new Segment(g.getPos(), moveTarget);
      for (int x = 0; x < xCellsCount; ++x) {
         for (int y = 0; y < yCellsCount; ++y) {
            CellInfo c = cells[x][y];

            double dist = moveSegment.distanceTo(c.getCenterOfCell());
            if (dist < g.getGroupRadius() + cellRadius) {
               for (VehicleType vt : vehicleTypes) {
                  double speed = Utils.getSpeed(vt) * getSpeedFactor(vt, c.getCenterOfCell());
                  if (speed < minSpeed) {
                     minSpeed = speed;
                  }
               }
            }
         }
      }

      return minSpeed;
   }

   private Facility getFacilityInCell(CellInfo c) {
      for (Facility f : facilities) {
         if (isFacilityCell(f, c)) {
            return f;
         }
      }
      return null;
   }

   private double calcDistFromOtherScoutsScore(Group g, CellInfo c) {
      double res = 0;
      for (Group group : groups) {
         if (!group.isScout() || group == g) {
            continue;
         }

         double dist = c.getCenterOfCell().distanceTo(group.getMoveTargetWithShiftOrPos());
         double s = Utils.linearInterpolate(dist, 0, fieldDiagonalLength, 7, 0);
         res -= s*s;
      }
      return res;
   }


   private void registerGroups(List<Group> groups) {
      for (Group g : groups) {
         registerGroup(g);
      }
   }

   private void addUnit(BattleUnit u) {
      CellInfo c = getCellForPos(u.getPos());
      if (!u.isMy()) {
         cellsWithEnemyUnits.add(c);
      }
      c.addUnit(u);
   }

   private void registerGroup(Group g) {
      CellInfo c = getCellOfGroup(g);
      c.addGroup(g);
   }

   private CellInfo getCellForPos(Point pos) {
      pos = Utils.ensureInBoundsOfField(pos);
      int x = (int) (pos.getX() / cellSize);
      int y = (int) (pos.getY() / cellSize);
      return cells[x][y];
   }

   public void onBeforeMove() {
      this.tick = world.getTick();

      aggressiveMode = false;
      if (tick > 10000 && world.getFacilities().size() > 0) {
          if (world.getMyUnits().size() > 300 + world.getFacilities().size() * 40) {
             aggressiveMode = true;
          }
      }

      cleanup();

      addUnits(world.getMyUnits());
      addUnits(world.getEnemyUnits());
      addVision(world.getMyUnits());
      registerGroups(groups);
   }

   private void cleanup() {
      for (int x = 0; x < xCellsCount; ++x) {
         for (int y = 0; y < yCellsCount; ++y) {
            cells[x][y].clear();
         }
      }
      cellsWithEnemyUnits.clear();
      nuclearStrikeScore.clear();
      Arrays.fill(scoutScore, -1);
   }

}
