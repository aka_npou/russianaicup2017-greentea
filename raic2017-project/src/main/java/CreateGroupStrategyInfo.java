/**
 * User: goodg_000
 * Date: 26.11.2017
 * Time: 15:41
 */
public class CreateGroupStrategyInfo extends GroupStrategyInfo {
   private boolean movedOutFromFactory;

   public boolean isMovedOutFromFactory() {
      return movedOutFromFactory;
   }

   public void setMovedOutFromFactory(boolean movedOutFromFactory) {
      this.movedOutFromFactory = movedOutFromFactory;
   }
}
