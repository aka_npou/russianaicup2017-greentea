/**
 * User: goodg_000
 * Date: 19.11.2017
 * Time: 13:02
 */
public enum BattleStrategyType {

   AvoidNuclearStrike,
   CreateGroups,
   CreateScouts,
   DoNuclearStrike,
   FactoryProduction,
   JoinGroups,
   Pack,
   TurnToEnemy,
   JoinCollidedGroups,
   CoptersStrategy,
   FightersStrategy,
   FactoryDefence,
   FacilityCapture,
   KillProduction,
   PotentialField;

   public int getPriority() {
      return ordinal();
   }
}
