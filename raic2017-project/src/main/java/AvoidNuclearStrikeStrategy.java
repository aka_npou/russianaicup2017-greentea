import model.*;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * User: goodg_000
 * Date: 19.11.2017
 * Time: 13:07
 */
public class AvoidNuclearStrikeStrategy extends BattleStrategy<AvoidNuclearStrikeStrategyInfo> {

   enum SelectionResult {
      NotAffected,
      Selected,
      AlreadySelected
   }

   private Point lastNuclearStrikeTarget;
   private NuclearStrikeDodgePhase nuclearStrikeDodgePhase;
   private int startRunFromNuclearStrikeTick;
   private int endBackFromNuclearStrikeTick;
   private boolean secondRunApplied = false;

   private List<BattleUnit> movedUnits = new ArrayList<>();

   public AvoidNuclearStrikeStrategy(Game game, WorldInfo world, List<Group> groups, Battlefield battlefield) {
      super(game, world, groups, battlefield,true);
   }

   @Override
   public boolean apply(Move move) {

      Player enemyPlayer = world.getEnemyPlayer();
      int tick = world.getTick();

      if (enemyPlayer.getNextNuclearStrikeVehicleId() != -1) {
         lastNuclearStrikeTarget = new Point(enemyPlayer.getNextNuclearStrikeX(), enemyPlayer.getNextNuclearStrikeY());
         if (nuclearStrikeDodgePhase == null) {
            SelectionResult sr = selectUnitsInNuclearStrikeRadius(lastNuclearStrikeTarget, move);
            if (sr.equals(SelectionResult.Selected)) {
               return true;
            }
            else if (sr.equals(SelectionResult.NotAffected)) {
               return false;
            }
            else if (sr.equals(SelectionResult.AlreadySelected)) {
               startRunFromNuclearStrikeTick = tick;
               runFromNuclearStrike(move);
               return true;
            }
         }
         else if (NuclearStrikeDodgePhase.Selected.equals(nuclearStrikeDodgePhase)) {
            startRunFromNuclearStrikeTick = tick;
            runFromNuclearStrike(move);
            return true;
         }
         else if (NuclearStrikeDodgePhase.Run.equals(nuclearStrikeDodgePhase)) {
            if (isHasNotMovedUnits() && !secondRunApplied) {
               secondRunApplied = true;
               runFromNuclearStrike(move);
            }

            return true;
         }
      }
      else if (NuclearStrikeDodgePhase.Run.equals(nuclearStrikeDodgePhase)) {
         backFromNuclearStrike(move);
         nuclearStrikeDodgePhase = NuclearStrikeDodgePhase.Back;
         endBackFromNuclearStrikeTick = world.getTick() + (world.getTick() - startRunFromNuclearStrikeTick);
         return true;
      }
      else if (NuclearStrikeDodgePhase.Back.equals(nuclearStrikeDodgePhase)) {
         if (tick >= endBackFromNuclearStrikeTick) {
            nuclearStrikeDodgePhase = null;
            finishStrategyForGroups();
         }
      }
      else if (nuclearStrikeDodgePhase != null) {
         nuclearStrikeDodgePhase = null;
         finishStrategyForGroups();
      }

      return false;
   }

   private boolean isHasNotMovedUnits() {
      for (BattleUnit u : movedUnits) {
         if (u.getVelocity().length() < Utils.SMALL_DOUBLE) {
            return true;
         }
      }
      return false;
   }

   private SelectionResult selectUnitsInNuclearStrikeRadius(Point nuclearStrikeTarget, Move move) {
      List<BattleUnit> unitsInDanger = Utils.filterUnitsInRange(world.getMyUnits(), nuclearStrikeTarget,
              game.getTacticalNuclearStrikeRadius());
      if (unitsInDanger.isEmpty()) {
         return SelectionResult.NotAffected;
      }
      Set<Group> groupsAffectedByExplosion = new HashSet<>();
      for (BattleUnit u : unitsInDanger) {
         groupsAffectedByExplosion.addAll(u.findGroupsOfUnit(groups));
      }

      movedUnits.clear();
      boolean hasUnitsWithoutGroup = false;
      for (Group g : groupsAffectedByExplosion) {
         setStrategyForGroup(g);
         movedUnits.addAll(g.getUnits());
         g.setMoveTarget(null);
      }
      for (BattleUnit u : unitsInDanger) {
         if (u.isWithoutGroup()) {
            movedUnits.add(u);
            hasUnitsWithoutGroup = true;
         }
      }

      if (groupsAffectedByExplosion.size() == 1 && !hasUnitsWithoutGroup
              && groupsAffectedByExplosion.iterator().next().isSelected()) {
         nuclearStrikeDodgePhase = NuclearStrikeDodgePhase.Selected;
         return SelectionResult.AlreadySelected;
      }

      for (Group g : groups) {
         g.setSelected(false);
      }

      RectSelection s = RectSelection.getSelectionForUnits(movedUnits);
      move.setAction(ActionType.CLEAR_AND_SELECT);
      s.updateMove(move);
      nuclearStrikeDodgePhase = NuclearStrikeDodgePhase.Selected;
      secondRunApplied = false;
      return SelectionResult.Selected;
   }

   private void runFromNuclearStrike(Move move) {
      move.setAction(ActionType.SCALE);
      move.setFactor(10);
      move.setX(lastNuclearStrikeTarget.getX());
      move.setY(lastNuclearStrikeTarget.getY());
      nuclearStrikeDodgePhase = NuclearStrikeDodgePhase.Run;
   }

   private void backFromNuclearStrike(Move move) {
      move.setAction(ActionType.SCALE);
      move.setFactor(0.1);
      move.setX(lastNuclearStrikeTarget.getX());
      move.setY(lastNuclearStrikeTarget.getY());
      nuclearStrikeDodgePhase = null;
      lastNuclearStrikeTarget = null;
   }

   @Override
   public BattleStrategyType getType() {
      return BattleStrategyType.AvoidNuclearStrike;
   }

   @Override
   protected AvoidNuclearStrikeStrategyInfo createGroupStrategyInfo() {
      return new AvoidNuclearStrikeStrategyInfo();
   }
}
