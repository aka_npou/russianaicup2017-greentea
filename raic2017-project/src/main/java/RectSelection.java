import model.Facility;
import model.Game;
import model.Move;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * User: goodg_000
 * Date: 08.11.2017
 * Time: 23:44
 */
public class RectSelection {
   private static double MARGIN = Utils.SMALL_DOUBLE;

   public static RectSelection getMinSelectionForUnit(BattleUnit u) {
      Point pos = u.getPos();
      return new RectSelection(
              pos.getX() - MARGIN, pos.getX() + MARGIN,
              pos.getY() - MARGIN, pos.getY() + MARGIN);
   }

   public static RectSelection getSelectionForUnits(Collection<BattleUnit> units) {
      double minX = Integer.MAX_VALUE;
      double minY = Integer.MAX_VALUE;
      double maxX = Integer.MIN_VALUE;
      double maxY = Integer.MIN_VALUE;
      for (BattleUnit u : units) {
         Point pos = u.getPos();
         if (pos.getX() < minX) {
            minX = pos.getX();
         }
         if (pos.getX() > maxX) {
            maxX = pos.getX();
         }
         if (pos.getY() < minY) {
            minY = pos.getY();
         }
         if (pos.getY() > maxY) {
            maxY = pos.getY();
         }
      }

      minX -= MARGIN;
      minY -= MARGIN;
      maxX += MARGIN;
      maxY += MARGIN;
      return new RectSelection(minX, maxX, minY, maxY);
   }

   public static RectSelection getFacilitySelection(Facility f) {
      Game game = Army.game;
      double minX = f.getLeft();
      double maxX = minX + game.getFacilityWidth();
      double minY = f.getTop();
      double maxY = minY + game.getFacilityHeight();
      return new RectSelection(minX, maxX, minY, maxY);
   }

   private double fromX;
   private double toX;
   private double fromY;
   private double toY;

   public RectSelection(double fromX, double toX, double fromY, double toY) {
      this.fromX = fromX;
      this.toX = toX;
      this.fromY = fromY;
      this.toY = toY;
   }

   public List<BattleUnit> filterUnitsInSelection(List<BattleUnit> units) {
      List<BattleUnit> res = new ArrayList<>();
      for (BattleUnit u : units) {
         if (isInSelection(u)) {
            res.add(u);
         }
      }
      return res;
   }

   public boolean isInSelection(BattleUnit u) {
      Point pos = u.getPos();
      double x = pos.getX();
      double y = pos.getY();
      return x > fromX && x < toX && y > fromY && y < toY;
   }

   public boolean isInSelection(Point pos) {
      double x = pos.getX();
      double y = pos.getY();
      return x > fromX && x < toX && y > fromY && y < toY;
   }

   public List<RectSelection> splitOn4Parts() {
      double middleX = (fromX + toX) / 2;
      double middleY = (fromY + toY) / 2;
      List<RectSelection> res = new ArrayList<>();
      res.add(new RectSelection(fromX, middleX, fromY, middleY));
      res.add(new RectSelection(fromX, middleX, middleY, toY));
      res.add(new RectSelection(middleX, toX, fromY, middleY));
      res.add(new RectSelection(middleX, toX, middleY, toY));
      return res;
   }

   public List<RectSelection> splitOn2Parts() {
      double middleY = (fromY + toY) / 2;
      List<RectSelection> res = new ArrayList<>();
      res.add(new RectSelection(fromX, toX, fromY, middleY));
      res.add(new RectSelection(fromX, toX, middleY, toY));
      return res;
   }

   public List<Segment> getBounds() {
      List<Segment> res = new ArrayList<>();
      Point p1 = new Point(fromX, fromY);
      Point p2 =  new Point(toX, fromY);
      Point p3 =  new Point(toX, toY);
      Point p4 =  new Point(fromX, toY);

      res.add(new Segment(p1, p2));
      res.add(new Segment(p2, p3));
      res.add(new Segment(p3, p4));
      res.add(new Segment(p4, p1));
      return res;
   }

   public void setFromPoint(Point p) {
      fromX = p.getX();
      fromY = p.getY();
   }

   public void setToPoint(Point p) {
      toX = p.getX();
      toY = p.getY();
   }

   public void updateMove(Move move) {
      move.setLeft(fromX);
      move.setRight(toX);
      move.setTop(fromY);
      move.setBottom(toY);
   }

   public double getFromX() {
      return fromX;
   }

   public double getToX() {
      return toX;
   }

   public double getFromY() {
      return fromY;
   }

   public double getToY() {
      return toY;
   }

   public double getWidth() {
      return toX - fromX;
   }

   public double getHeight() {
      return toY- fromY;
   }
}
