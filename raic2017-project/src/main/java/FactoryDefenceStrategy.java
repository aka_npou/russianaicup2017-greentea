import model.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * User: goodg_000
 * Date: 01.12.2017
 * Time: 22:45
 */
public class FactoryDefenceStrategy extends BattleStrategy<FactoryDefenceStrategyInfo> {

   private static final double FACTORY_DANGER_RADIUS = 200;
   private static final double MAX_DEFENCE_RADIUS = 350;

   public FactoryDefenceStrategy(Game game, WorldInfo world, List<Group> groups, Battlefield battlefield) {
      super(game, world, groups, battlefield, false);
   }

   @Override
   protected boolean apply(Move move) {
      Map<Long, FactoryDefenceInfo> factoryDefenceInfos = new HashMap<>();
      for (Facility f : world.getFacilities()) {
         factoryDefenceInfos.put(f.getId(), new FactoryDefenceInfo());
      }

      for (Group g : getCurrentStrategyGroups()) {
         FactoryDefenceStrategyInfo si = getGroupStrategyInfo(g);
         factoryDefenceInfos.get(si.getTargetFacilityId()).addDefender(g);
      }

      Facility bestFacilityForDefend = null;
      Group bestGroupToDefend = null;
      double maxScore = Integer.MIN_VALUE;
      Point defenceTarget = null;

      for (Facility f : world.getFacilities()) {
         if (!f.getType().equals(FacilityType.VEHICLE_FACTORY)) {
            continue;
         }

         if (f.getOwnerPlayerId() != world.getMyPlayerId() && !(Utils.isCapturedByMe(f) && Utils.isCapturedByEnemy(f))) {
            continue;
         }

         if (factoryDefenceInfos.get(f.getId()).getDefenders().size() > 0) {
            continue;
         }

         Point facilityCenter = battlefield.getFacilityCenter(f);
         List<BattleUnit> enemies = battlefield.getEnemiesInRange(facilityCenter,
                 FACTORY_DANGER_RADIUS);
         enemies = Utils.filterGroundUnits(enemies);
         if (enemies.isEmpty()) {
            continue;
         }
         if (!isRequireDefence(f, battlefield)) {
            continue;
         }

         VehicleType enemyVehicleType = Utils.getVehicleTypeWithMaxUnits(enemies);
         if (Utils.isAirUnit(enemyVehicleType) && (f.getVehicleType() == null
                 || !battlefield.isCanAttack(enemyVehicleType, f.getVehicleType()))) {
            continue;
         }

         Point target = Utils.findNearest(facilityCenter, enemies).getPos();

         for (Group g : getStrategyCandidatesGroups()) {
//            if (g.isHasGroundUnits() && g.isBigGroup()) {
//               continue;
//            }
            if (g.isHasGroundUnits() && (isCapturingFactory(g) || isCloserToCapturingFactory(f, g))) {
               continue;
            }

            double dist = g.getPos().distanceTo(target);
            if (dist > MAX_DEFENCE_RADIUS) {
               continue;
            }

            if (!battlefield.isCanAttack(g.getVehicleType(), enemyVehicleType)) {
               continue;
            }

            if (!battlefield.isCanSafelyMoveToPoint(g, target)) {
               continue;
            }

            double defenceScore = 10 * g.size() - dist;
            if (defenceScore > maxScore) {
               maxScore = defenceScore;
               bestFacilityForDefend = f;
               bestGroupToDefend = g;
               defenceTarget = target;
            }
         }
      }

      if (bestFacilityForDefend != null) {
         final Group g = bestGroupToDefend;
         final Point target = defenceTarget;
         final Facility f = bestFacilityForDefend;
         setStrategyForGroup(g).setTargetFacilityId(bestFacilityForDefend.getId());
         return selectAndCallTrigger(g, move, new SelectGroupActionStrategyTrigger() {
            @Override
            public boolean apply(Move move) {
               Point targetWithShift = battlefield.getTargetWithShift(g, target);
               performMoveAction(g, target, targetWithShift, move);
               getGroupStrategyInfo(g).setTrigger(new GroupStrategyTrigger() {
                  @Override
                  public boolean isShouldApply() {
                     double dist = g.getPos().distanceTo(g.getMoveTargetWithShiftOrPos());
                     if (dist < 10) {
                        return true;
                     }

                     if (!battlefield.isCanSafelyMoveToPoint(g, target)) {
                        return true;
                     }

                     List<BattleUnit> enemies = battlefield.getEnemiesInRange(battlefield.getFacilityCenter(f),
                             FACTORY_DANGER_RADIUS);
                     List<BattleUnit> groundEnemies = Utils.filterGroundUnits(enemies);
                     if (groundEnemies.isEmpty()) {
                        return true;
                     }

                     return false;
                  }

                  @Override
                  public boolean apply(Move move) {
                     finishStrategyForGroup(g);
                     return false;
                  }
               });

               return true;
            }
         });
      }

      return false;
   }

   private boolean isCapturingFactory(Group g) {
      for (Facility f : world.getFacilities()) {
         if (f.getType().equals(FacilityType.VEHICLE_FACTORY) && f.getOwnerPlayerId() != world.getMyPlayerId()) {
            if (battlefield.getFacilityCenter(f).distanceTo(g.getPos()) < game.getFacilityWidth()/2) {
               return true;
            }
         }
      }
      return false;
   }

   private boolean isCloserToCapturingFactory(Facility factoryToDefend, Group g) {
      FacilityCaptureStrategyInfo si = (FacilityCaptureStrategyInfo) g.getGroupStrategyInfo(
              BattleStrategyType.FacilityCapture);

      if (si != null) {
         Facility facilityToCapture = world.getFacility(si.getTargetFacilityId());
         if (facilityToCapture != null && facilityToCapture.getType().equals(FacilityType.VEHICLE_FACTORY)) {
            Point attackTarget = battlefield.getFacilityCenter(facilityToCapture);
            if (battlefield.isCanSafelyMoveToPoint(g, attackTarget)) {
               Point defenceTarget = battlefield.getFacilityCenter(factoryToDefend);
               if (g.getPos().distanceTo(attackTarget) < g.getPos().distanceTo(defenceTarget)) {
                  return true;
               }
            }

         }
      }

      return false;
   }

   public static boolean isRequireDefence(Facility f, Battlefield battlefield) {
      Point facilityCenter = battlefield.getFacilityCenter(f);
      List<BattleUnit> enemies = battlefield.getEnemiesInRange(facilityCenter,
              FACTORY_DANGER_RADIUS);
      enemies = Utils.filterGroundUnits(enemies);
      if (enemies.isEmpty()) {
         return false;
      }

      if (f.getVehicleType() == VehicleType.TANK && enemies.size() <= 8) {
         return false;
      }
      if (f.getVehicleType() != VehicleType.TANK) {
         return true;
      }

      Point averageEnemyPos = Utils.calcAveragePos(Utils.getPositionsOfUnits(enemies));
      VehicleType enemyType = Utils.getVehicleTypeWithMaxUnits(enemies);
      double distFromEnemyToFacility = battlefield.getFacilityCenter(f).distanceTo(averageEnemyPos);
      double timeEnemyGoToFacility = distFromEnemyToFacility / Utils.getSpeed(enemyType);

      List<BattleUnit> defenders = RectSelection.getFacilitySelection(f).filterUnitsInSelection(
              Army.world.getMyFreeUnits());
      int newUnitsProduced = (int)(timeEnemyGoToFacility / Army.game.getTankProductionCost());
      for (int i = 0; i < newUnitsProduced; ++i) {
         defenders.add(new BattleUnit(f.getVehicleType()));
      }

      if (!battlefield.isCanDefend(defenders, f.getVehicleType(), enemies)) {
         return true;
      }
      return false;

//      long defendTime = battlefield.calcDefendTime(defenders, f.getVehicleType(), enemies);
//      long ticksToCapture = battlefield.getTicksToCapture(f, false);
//      return ticksToCapture >= 0 && defendTime > ticksToCapture;
   }

   @Override
   public BattleStrategyType getType() {
      return BattleStrategyType.FactoryDefence;
   }

   @Override
   protected FactoryDefenceStrategyInfo createGroupStrategyInfo() {
      return new FactoryDefenceStrategyInfo();
   }
}
