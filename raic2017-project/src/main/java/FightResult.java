/**
 * User: goodg_000
 * Date: 10.12.2017
 * Time: 19:44
 */
public class FightResult {
   private final double killedMinusLost;

   private final UnitsCounts u1;
   private final UnitsCounts u2;

   private final int endRound;

   public FightResult(double killedMinusLost, UnitsCounts u1, UnitsCounts u2, int endRound) {
      this.killedMinusLost = killedMinusLost;
      this.u1 = u1;
      this.u2 = u2;
      this.endRound = endRound;
   }

   public double getKilledMinusLost() {
      return killedMinusLost;
   }

   public UnitsCounts getU1() {
      return u1;
   }

   public UnitsCounts getU2() {
      return u2;
   }

   public int getEndRound() {
      return endRound;
   }
}
