import model.VehicleType;

/**
 * User: goodg_000
 * Date: 11.11.2017
 * Time: 19:25
 */
public class DamageMatrix {
   double[][] damage = new double[VehicleType.values().length][VehicleType.values().length];

   DamageMatrix(double[] damageAgainstGround, double[] damageAgainstAir,
                double[] defenceAgainstGround, double[] defenceAgainstAir) {
      for (VehicleType t1 : VehicleType.values()) {
         for (VehicleType t2 : VehicleType.values()) {
            int i1 = t1.ordinal();
            int i2 = t2.ordinal();

            double damage = 0;
            if (t2.equals(VehicleType.HELICOPTER) || t2.equals(VehicleType.FIGHTER)) {
               damage = damageAgainstAir[i1];
            }
            else {
               damage = damageAgainstGround[i1];
            }

            double defence = 0;
            if (t1.equals(VehicleType.HELICOPTER) || t1.equals(VehicleType.FIGHTER)) {
               defence = defenceAgainstAir[i2];
            }
            else {
               defence = defenceAgainstGround[i2];
            }

            this.damage[i1][i2] = Math.max(0.01 * (damage - defence), 0);
         }
      }
   }

   public double getDamage(VehicleType fromType, VehicleType toType) {
      return damage[fromType.ordinal()][toType.ordinal()];
   }
}
