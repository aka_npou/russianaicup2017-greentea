import model.*;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * User: goodg_000
 * Date: 26.11.2017
 * Time: 13:54
 */
public class FactoryProductionStrategy extends BattleStrategy<GroupStrategyInfo> {

   private static final int MIN_TICKS_TO_SWITCH_PRODUCTION = 300;

   private static Map<Long, FactoryProductionInfo> productionInfo = new HashMap<>();

   public static FactoryProductionInfo getProductionInfo(Facility f) {
      return productionInfo.get(f.getId());
   }

   public FactoryProductionStrategy(Game game, WorldInfo world, List<Group> groups, Battlefield battlefield) {
      super(game, world, groups, battlefield,true);
   }

   @Override
   protected boolean apply(Move move) {
      for (Facility f : world.getFacilities()) {
         if (f.getOwnerPlayerId() != world.getMyPlayer().getId() && productionInfo.containsKey(f.getId())) {
            productionInfo.remove(f.getId());
         }

         if (f.getOwnerPlayerId() == world.getMyPlayer().getId() && f.getType().equals(FacilityType.VEHICLE_FACTORY)) {
            FactoryProductionInfo factoryProductionInfo = productionInfo.get(f.getId());
            if (factoryProductionInfo == null) {
               return orderUnitsOnFactory(f, findBestUnitsToProduce(), move);
            }

            if (world.getTick() > factoryProductionInfo.getProductionStartTick() + MIN_TICKS_TO_SWITCH_PRODUCTION) {
               int freeUnitsInFactory = RectSelection.getFacilitySelection(f).filterUnitsInSelection(
                       world.getMyFreeUnits()).size();
               if (freeUnitsInFactory <= 2) {
                  VehicleType vehicleType = findCounterUnitType(f);
                  if (vehicleType == null) {
                     vehicleType = findBestUnitsToProduce();
                  }
                  if (!vehicleType.equals(f.getVehicleType())) {
                     return orderUnitsOnFactory(f, vehicleType, move);
                  }
               }
            }
         }
      }
      return false;
   }

   private boolean orderUnitsOnFactory(Facility f, VehicleType vehicleType, Move move) {
      move.setFacilityId(f.getId());
      move.setVehicleType(vehicleType);
      move.setAction(ActionType.SETUP_VEHICLE_PRODUCTION);
      productionInfo.put(f.getId(), new FactoryProductionInfo(world.getTick(), vehicleType));
      return true;
   }

   private VehicleType findCounterUnitType(Facility f) {
      if (f.getVehicleType() == null) {
         return null;
      }

      Point facilityCenter = battlefield.getFacilityCenter(f);
      List<BattleUnit> closeEnemyUnits = Utils.filterUnitsInRange(world.getEnemyUnits(),
              facilityCenter, 100);
      if (closeEnemyUnits.isEmpty()) {
         return null;
      }

      VehicleType enemyUnitsType = Utils.getVehicleTypeWithMaxUnits(closeEnemyUnits);
      VehicleType res = null;
      if (enemyUnitsType.equals(VehicleType.HELICOPTER)) {
         res = VehicleType.FIGHTER;
      }
      if (Utils.isAirUnit(f.getVehicleType()) && enemyUnitsType.equals(VehicleType.FIGHTER)) {
         res = VehicleType.TANK;
      }
      if (f.getVehicleType().equals(VehicleType.IFV) && !Utils.isAirUnit(enemyUnitsType)) {
         res = VehicleType.TANK;
      }
      return res;

//      List<VehicleType> counterUnitTypes = Utils.getCounterUnitTypes(enemyUnitsType);
//      if (counterUnitTypes.contains(f.getVehicleType())) {
//         return null;
//      }
//
//      return counterUnitTypes.get(0);
   }

   int counter = 0;
//   List<VehicleType> vehicleTypes = Arrays.asList(VehicleType.TANK, VehicleType.TANK, VehicleType.TANK, VehicleType.TANK,
//           VehicleType.HELICOPTER, VehicleType.HELICOPTER, VehicleType.FIGHTER);
   List<VehicleType> vehicleTypes = Arrays.asList(VehicleType.TANK, VehicleType.TANK, VehicleType.HELICOPTER, VehicleType.IFV);
   private VehicleType findBestUnitsToProduce() {
//      VehicleType vehicleType = VehicleType.TANK;
//      if (world.getEnemyUnits().stream().filter(u -> u.getType().equals(VehicleType.HELICOPTER)).count() >
//              world.getMyBmps().size()) {
//         vehicleType = VehicleType.IFV;
//      }

//      VehicleType vehicleType = vehicleTypes.get(counter);
//      counter = (counter + 1) % vehicleTypes.size();

      VehicleType vehicleType = VehicleType.TANK;
      if (world.getTick() > 3000) {
         if (world.getEnemyUnitsOfType(VehicleType.FIGHTER).size() + world.getEnemyUnitsOfType(VehicleType.HELICOPTER).size()/2 >
                 world.getMyUnitsOfType(VehicleType.FIGHTER).size()) {
            vehicleType = VehicleType.FIGHTER;
         }
      }

//      VehicleType vehicleType = VehicleType.HELICOPTER;
//      if (world.getEnemyUnitsOfType(VehicleType.FIGHTER).size() >
//              world.getMyUnitsOfType(VehicleType.FIGHTER).size()) {
//         vehicleType = VehicleType.FIGHTER;
//      }
//      else if (world.getMyUnitsOfType(VehicleType.TANK).size() < world.getMyUnitsOfType(VehicleType.HELICOPTER).size()) {
//         vehicleType = VehicleType.TANK;
//      }
      return vehicleType;
   }

   @Override
   public BattleStrategyType getType() {
      return BattleStrategyType.FactoryProduction;
   }

   @Override
   protected GroupStrategyInfo createGroupStrategyInfo() {
      return new GroupStrategyInfo();
   }
}
