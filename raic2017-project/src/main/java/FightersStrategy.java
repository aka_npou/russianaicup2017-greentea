import model.Game;
import model.Move;
import model.VehicleType;

import java.util.List;

/**
 * User: goodg_000
 * Date: 16.12.2017
 * Time: 15:17
 */
public class FightersStrategy extends BattleStrategy<FightersStrategyInfo> {

   private static class MoveTarget {
      private final Point pos;
      private final double score;

      public MoveTarget(Point pos, double score) {
         this.pos = pos;
         this.score = score;
      }
   }

   public FightersStrategy(Game game, WorldInfo world, List<Group> groups, Battlefield battlefield) {
      super(game, world, groups, battlefield, false);
   }

   @Override
   protected boolean apply(Move move) {
      Group fighters = getFightersWithMaxUnits();

      if (fighters == null) {
         return false;
      }

      List<BattleUnit> enemyCopters = world.getEnemyUnitsOfType(VehicleType.HELICOPTER);
      if (enemyCopters.isEmpty()) {
         finishStrategyForGroup(fighters);
         return false;
      }

      MoveTarget bestMoveTarget = createAttackTarget(fighters, enemyCopters);

      Point enemyCoptersPos = Utils.calcAveragePos(Utils.getPositionsOfUnits(enemyCopters));
      for (Group g : groups) {
         if ((g.getVehicleType().equals(VehicleType.TANK) && enemyCopters.size() * 1.5 > g.size()) ||
                 g.getVehicleType().equals(VehicleType.ARRV)) {
            MoveTarget mt = createMoveTarget(fighters, g, enemyCoptersPos);
            if (mt != null && (bestMoveTarget == null || mt.score > bestMoveTarget.score)) {
               bestMoveTarget = mt;
            }
         }
      }

      if (bestMoveTarget != null) {
         boolean requireMove = false;
         if (isCurrentStrategy(fighters)) {
            if (bestMoveTarget.pos.distanceTo(fighters.getMoveTargetWithShiftOrPos()) > fighters.getGroupRadius()*2) {
               requireMove = true;
            }
         }
         else {
            requireMove = true;
         }

         if (requireMove) {
            final Group g = fighters;
            final MoveTarget mt = bestMoveTarget;
            return selectAndCallTrigger(fighters, move, new SelectGroupActionStrategyTrigger() {
               @Override
               public boolean apply(Move move) {
                  performMoveAction(g, mt.pos, mt.pos, move);
                  getGroupStrategyInfo(g).setTrigger(new GroupStrategyTrigger() {
                     @Override
                     public boolean isShouldApply() {
                        return g.getPos().distanceTo(mt.pos) < g.getGroupRadius() ||
                                !battlefield.isCanSafelyMoveToPoint(g, mt.pos);
                     }

                     @Override
                     public boolean apply(Move move) {
                        finishStrategyForGroup(g);
                        return false;
                     }
                  });
                  return true;
               }
            });
         }
      }

      return false;
   }

   private Group getFightersWithMaxUnits() {
      Group res = null;
      for (Group g : groups) {
         if (g.getVehicleType().equals(VehicleType.FIGHTER) && !g.isScout() && !isGroupBusy(g)) {
            if (res == null || g.size() > res.size()) {
               res = g;
            }
         }
      }
      return res;
   }

   private MoveTarget createMoveTarget(Group fighters, Group groupToDefend, Point enemyCoptersPos) {
      if (groupToDefend.size() <= 10) {
         return null;
      }

      final double reactionTime = 20;
      double distCoptersToTarget = enemyCoptersPos.distanceTo(groupToDefend.getPos());
      double distFightersToTarget = fighters.getPos().distanceTo(groupToDefend.getPos());
      double timeCoptersToTarget = distCoptersToTarget / Utils.getSpeed(VehicleType.HELICOPTER);
      double timeFightersToTarget = (distFightersToTarget / Utils.getSpeed(VehicleType.FIGHTER)) + reactionTime;
      boolean targetInDanger = timeCoptersToTarget < timeFightersToTarget;
      boolean canDefendTarget = targetInDanger && battlefield.isCanSafelyMoveToPoint(fighters, groupToDefend.getPos());
      if (!canDefendTarget) {
         return null;
      }

      double score = 10*groupToDefend.size() - enemyCoptersPos.distanceTo(groupToDefend.getPos());
      return new MoveTarget(groupToDefend.getPos(), score);
   }

   private MoveTarget createAttackTarget(Group fighters, List<BattleUnit> enemyCopters) {
      Point pos = fighters.getPos();
      double minDist = Integer.MAX_VALUE;
      BattleUnit nearestCopter = null;
      for (BattleUnit c : enemyCopters) {
         double dist = pos.distanceTo(c.getPos());
         if (dist < minDist) {
            minDist = dist;
            nearestCopter = c;
         }
      }

      //Point targetWithShift = battlefield.getTargetWithShift(fighters, nearestCopter.getPos());
      Point targetWithShift = nearestCopter.getPos();

      if (!battlefield.isCanSafelyMoveToPoint2(fighters, targetWithShift)) {
         return null;
      }

      double score = 20*enemyCopters.size() - pos.distanceTo(targetWithShift);
      return new MoveTarget(targetWithShift, score);
   }

   @Override
   public BattleStrategyType getType() {
      return BattleStrategyType.FightersStrategy;
   }

   @Override
   protected FightersStrategyInfo createGroupStrategyInfo() {
      return new FightersStrategyInfo();
   }
}
