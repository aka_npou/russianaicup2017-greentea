import model.*;

import java.util.*;

/**
 * User: goodg_000
 * Date: 07.11.2017
 * Time: 21:08
 */
public class WorldInfo {
   public static final int NEUTRAL_PLAYER_ID = -1;

   // static info
   private final int ticksPerGame;
   private final double width;
   private final double height;

   private final TerrainType[][] terrains;
   private final WeatherType[][] weathers;

   // dynamic info
   private int tick;

   private Player myPlayer;
   private Player enemyPlayer;
   private Map<Long, BattleUnit> battleUnitsMap = new HashMap<>(2000);

   private List<BattleUnit> myUnits = new ArrayList<>();
   private Map<VehicleType, List<BattleUnit>> myUnitsByType = new EnumMap<>(VehicleType.class);

   private List<BattleUnit> myNewUnits = new ArrayList<>();
   private List<BattleUnit> myFreeUnits = new ArrayList<>();

   private List<BattleUnit> enemyUnits = new ArrayList<>();
   private Set<Long> enemyUnitsIds = new HashSet<>();
   private List<BattleUnit> hiddenEnemyUnits = new ArrayList<>();
   private Map<VehicleType, List<BattleUnit>> enemyUnitsByType = new EnumMap<>(VehicleType.class);

   private List<Facility> facilities = new ArrayList<>();
   private List<Facility> factories = new ArrayList<>();
   private Set<Long> myFacilities = new HashSet<>();

   public WorldInfo(World world) {
      this.ticksPerGame = world.getTickCount();
      this.width = world.getWidth();
      this.height = world.getHeight();
      this.terrains = world.getTerrainByCellXY();
      this.weathers = world.getWeatherByCellXY();

      if (Army.game.isFogOfWarEnabled()) {
         for (long i = 501; i <= 1000; ++i) {
            enemyUnitsIds.add(i);
         }
      }

      updateDynamicInfo(world);
   }

   void updateDynamicInfo(World world) {
      tick = world.getTickIndex();

      myNewUnits.clear();
      myFreeUnits.clear();
      myPlayer = world.getMyPlayer();
      enemyPlayer = world.getOpponentPlayer();

      for (Vehicle v : world.getNewVehicles()) {
         BattleUnit oldUnit = battleUnitsMap.get(v.getId());
         if (oldUnit != null && oldUnit.isHidden()) {
            hiddenEnemyUnits.remove(oldUnit);
            removeUnit(oldUnit);
         }

         BattleUnit u = new BattleUnit(v, world.getMyPlayer().getId());
         addUnit(u);
      }

      myUnits.forEach(BattleUnit::onNewTick);
      enemyUnits.forEach(BattleUnit::onNewTick);

      for (VehicleUpdate vu : world.getVehicleUpdates()) {
         BattleUnit u = battleUnitsMap.get(vu.getId());
         if (u.isMy()) {
            u.update(vu);
            if (u.isRemoved()) {
               removeUnit(u);
            }
         }
      }

      for (VehicleUpdate vu : world.getVehicleUpdates()) {
         BattleUnit u = battleUnitsMap.get(vu.getId());
         if (!u.isMy()) {
            if (vu.getDurability() == 0) {
               if (Army.game.isFogOfWarEnabled() && isHidden(vu)) {
                  u.setHidden(true);
                  hiddenEnemyUnits.add(u);
               }
               else {
                  u.update(vu);
                  removeUnit(u);
               }
            }
            else {
               u.update(vu);
            }
         }
      }

      List<BattleUnit> hiddenUnitsInVisionRange = new ArrayList<>();
      for (BattleUnit u : hiddenEnemyUnits) {
         if (isVisible(u.getPos())) {
            removeUnit(u);
            hiddenUnitsInVisionRange.add(u);
         }
      }
      if (!hiddenUnitsInVisionRange.isEmpty()) {
         hiddenEnemyUnits.removeAll(hiddenUnitsInVisionRange);
      }

      fillUnitsByType(myUnits, myUnitsByType);
      fillUnitsByType(enemyUnits, enemyUnitsByType);

      facilities.clear();
      factories.clear();
      Collections.addAll(facilities, world.getFacilities());
      myFacilities.clear();
      for (Facility f : facilities) {
         if (f.getOwnerPlayerId() == myPlayer.getId()) {
            myFacilities.add(f.getId());
         }
         if (f.getType() == FacilityType.VEHICLE_FACTORY) {
            factories.add(f);
         }
      }

      for (BattleUnit u : myUnits) {
         if (u.isWithoutGroup() && !CreateScoutsStrategy.isFutureScout(u)) {
            myFreeUnits.add(u);
         }
      }
   }

   private boolean isHidden(VehicleUpdate vu) {
      return vu.getDurability() == 0 && vu.getX() == 0 && vu.getY() == 0;
   }

   private boolean isVisible(Point pos) {
      for (BattleUnit u : myUnits) {
         if (isInVisionRange(u, pos)) {
            return true;
         }
      }
      return false;
   }

   private boolean isInVisionRange(BattleUnit u, Point pos) {
      double dist = u.getPos().distanceTo(pos);
      double visionRange = u.getVisionRange();
      return dist < visionRange;
   }

   private void fillUnitsByType(List<BattleUnit> units, Map<VehicleType, List<BattleUnit>> res) {
      res.clear();
      for (VehicleType vehicleType : VehicleType.values()) {
         res.put(vehicleType, new ArrayList<>());
      }

      for (BattleUnit u : units) {
         res.get(u.getType()).add(u);
      }
   }

   private void addUnit(BattleUnit u) {
      battleUnitsMap.put(u.getId(), u);
      if (u.isMy()) {
         myUnits.add(u);
         myNewUnits.add(u);
      }
      else {
         enemyUnits.add(u);
         enemyUnitsIds.add(u.getId());
      }
   }

   private void removeUnit(BattleUnit u) {
      if (u.isMy()) {
         myUnits.remove(u);
      }
      else {
         enemyUnits.remove(u);
         enemyUnitsIds.remove(u.getId());
      }
   }

   public double getWidth() {
      return width;
   }

   public double getHeight() {
      return height;
   }

   public int getTicksPerGame() {
      return ticksPerGame;
   }

   public TerrainType[][] getTerrains() {
      return terrains;
   }

   public WeatherType[][] getWeathers() {
      return weathers;
   }

   public int getTick() {
      return tick;
   }

   public Player getMyPlayer() {
      return myPlayer;
   }

   public long getMyPlayerId() {
      return myPlayer.getId();
   }

   public Player getEnemyPlayer() {
      return enemyPlayer;
   }

   public Map<Long, BattleUnit> getBattleUnitsMap() {
      return battleUnitsMap;
   }

   public List<BattleUnit> getMyUnits() {
      return myUnits;
   }

   public List<BattleUnit> getMyNewUnits() {
      return myNewUnits;
   }

   public List<BattleUnit> getEnemyUnits() {
      return enemyUnits;
   }


   public List<Facility> getFacilities() {
      return facilities;
   }

   public List<BattleUnit> getAllUnits() {
      List<BattleUnit> res = new ArrayList<>();
      res.addAll(myUnits);
      res.addAll(enemyUnits);
      return res;
   }

   public List<BattleUnit> getMyFreeUnits() {
      return myFreeUnits;
   }

   public boolean isMyFacility(Facility f) {
      return isMyFacility(f.getId());
   }

   public boolean isMyFacility(long facilityId) {
      return myFacilities.contains(facilityId);
   }

   public Facility getFacility(long facilityId) {
      for (Facility f : facilities) {
         if (f.getId() == facilityId) {
            return f;
         }
      }
      return null;
   }

   public List<Facility> getFactories() {
      return factories;
   }

   public List<BattleUnit> getMyUnitsOfType(VehicleType vehicleType) {
      return myUnitsByType.get(vehicleType);
   }

   public List<BattleUnit> getEnemyUnitsOfType(VehicleType vehicleType) {
      return enemyUnitsByType.get(vehicleType);
   }

   public int getEnemyUnitsCount() {
      return enemyUnitsIds.size();
   }
}
