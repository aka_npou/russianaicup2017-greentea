/**
 * User: goodg_000
 * Date: 23.11.2017
 * Time: 18:56
 */
public class GroupStrategyInfo {
   private int startStrategyTick;
   private int endStrategyTick;

   private GroupStrategyTrigger trigger;

   public int getStartStrategyTick() {
      return startStrategyTick;
   }

   public int getEndStrategyTick() {
      return endStrategyTick;
   }
   public void setEndStrategyTick(int endStrategyTick) {
      this.endStrategyTick = endStrategyTick;
   }

   public void restartStrategy() {
      startStrategyTick = Army.world.getTick();
      endStrategyTick = 0;
   }

   public GroupStrategyTrigger getTrigger() {
      return trigger;
   }

   public void setTrigger(GroupStrategyTrigger trigger) {
      this.trigger = trigger;
   }
}
