/**
 * User: goodg_000
 * Date: 01.12.2017
 * Time: 22:46
 */
public class FactoryDefenceStrategyInfo extends GroupStrategyInfo {
   private long targetFacilityId;

   public long getTargetFacilityId() {
      return targetFacilityId;
   }

   public void setTargetFacilityId(long targetFacilityId) {
      this.targetFacilityId = targetFacilityId;
   }
}
