import model.VehicleType;

import java.util.List;

/**
 * User: goodg_000
 * Date: 11.11.2017
 * Time: 19:12
 */
public class FightSimulator {

   private double HEAL_PER_ATTACK = 0.06 / 3;

   private DamageMatrix damageMatrix;

   public FightSimulator(DamageMatrix damageMatrix) {
      this.damageMatrix = damageMatrix;
   }

   public FightResult fight(UnitsCounts u1, UnitsCounts u2, VehicleType u1Produced, VehicleType u2Produced,
                            int maxRoundsCount) {
      u1 = u1.copy();
      u2 = u2.copy();
      double killedMinusLost = 0;

      int round = 1;
      while (u1.isNotEmpty() && u2.isNotEmpty() && round <= maxRoundsCount) {
         if (u1Produced != null && u1.get(u1Produced) > 0) {
            u1.add(u1Produced, 1);
         }
         if (u2Produced != null && u2.get(u2Produced) > 0) {
            u2.add(u2Produced, 1);
         }

         UnitsCounts u1AfterDefence = u1.copy();
         UnitsCounts u2AfterDefence = u2.copy();

         double h1 = u1.get(VehicleType.ARRV);
         double totalHeal1 = 0;
         if (h1 > 0) {
            totalHeal1 = heal(u1AfterDefence, h1);
         }
         double h2 = u2.get(VehicleType.ARRV);
         double totalHeal2 = 0;
         if (h2 > 0) {
            totalHeal2 = heal(u2AfterDefence, h2);
         }

         double damage1 = attack(u1, u2AfterDefence) - totalHeal2;
         double damage2 = attack(u2, u1AfterDefence) - totalHeal1;

         u1 = u1AfterDefence;
         u2 = u2AfterDefence;

         if (damage1 <= 0 && damage2 <= 0) {
            break;
         }

         killedMinusLost += (damage1 - damage2);

         round++;
      }

      return new FightResult(killedMinusLost, u1, u2, round);
   }

   public FightResult fight(UnitsCounts u1, UnitsCounts u2, VehicleType u1Produced, VehicleType u2Produced) {
      return fight(u1, u2, u1Produced, u2Produced,100);
   }

   public FightResult fight(UnitsCounts u1, UnitsCounts u2) {
      return fight(u1, u2, null, null,100);
   }

   public double fightSingleRound(UnitsCounts u1, UnitsCounts u2) {
      return fight(u1, u2, null, null,1).getKilledMinusLost();
   }

   private double heal(UnitsCounts units, double healers) {
      double totalHeal = healers * HEAL_PER_ATTACK;
      double totalUnits = units.getTotalUnitsCount();
      for (VehicleType type : VehicleType.values()) {
         double unitsOfType = units.get(type);
         if (unitsOfType > 0) {
            double healed = (unitsOfType / totalUnits) * totalHeal;
            units.add(type, healed);
         }
      }
      return totalHeal;
   }

   private double attack(UnitsCounts attackerUnits, UnitsCounts defenderUnits) {
      List<VehicleType> attackersUnitTypes = attackerUnits.getNonZeroVehicleTypes();
      List<VehicleType> defendersUnitTypes = defenderUnits.getNonZeroVehicleTypes();

      double totalDamage = 0;
      int selectTargetIndex = 0;
      A: for (VehicleType attackerType : attackersUnitTypes) {
         double attackers = attackerUnits.get(attackerType);

         for (int i = 0; i < attackers; ++i) {
            int tryCount = 0;
            VehicleType targetType = null;
            double damageCount = 0;
            while (tryCount < defendersUnitTypes.size()) {
               int j = selectTargetIndex % defendersUnitTypes.size();
               targetType = defendersUnitTypes.get(j);
               damageCount = damageMatrix.getDamage(attackerType, targetType);
               if (damageCount == 0) {
                  selectTargetIndex++;
                  tryCount++;
               }
               else {
                  break;
               }
            }

            if (damageCount == 0) {
               continue;
            }

            damageCount = Math.min(defenderUnits.get(targetType), damageCount);
            totalDamage += damageCount;
            if (defenderUnits.damage(targetType, damageCount)) {
               defendersUnitTypes.remove(targetType);
               if (defendersUnitTypes.isEmpty()) {
                  break A;
               }
            }

            selectTargetIndex++;
         }
      }

      return totalDamage;
   }

   public DamageMatrix getDamageMatrix() {
      return damageMatrix;
   }
}
